import 'dart:convert';
import 'package:tutorial_project/Theme.dart' as Theme;
import 'package:flutter/material.dart';
import 'package:tutorial_project/LanguagesList/languagesListScreen.dart';
import 'package:tutorial_project/SignUp/signUpScreen.dart';
import 'package:tutorial_project/api/apis.dart';
import 'package:shared_preferences/shared_preferences.dart';
class LogIn extends StatefulWidget {
  @override
  _LogInState createState() => _LogInState();
}

class _LogInState extends State<LogIn> {


  bool _isLoading = false;


  TextEditingController mailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  ScaffoldState scaffoldState;
  _showMsg(msg) { //
    final snackBar = SnackBar(
      content: Text(msg),
      action: SnackBarAction(
        label: '',
        onPressed: () {
          // Some code to undo the change!
        },
      ),
    );
    Scaffold.of(context).showSnackBar(snackBar);
   }
 @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        
        child: Stack(
          children: <Widget>[
            ///////////  background///////////
            new Container(
              decoration: new BoxDecoration(

                image: new DecorationImage(
                  image: new AssetImage("assets/ops/login1.jpeg"),
               
                  //image: new AssetImage("assets/images/login1.png"),
                  alignment: Alignment.bottomLeft,
                  fit: BoxFit.fitWidth,
                ),
                gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  stops: [0.0, 0.4, 0.9],
                  
                  colors: [
                    Color(0xFFeafaf1),
                    Color(0xFFffffff),
                    Color(0xFFeaeaea),                   
                  ],
                ),
              ),
            ),
             Positioned(
              child: Padding(

                padding: const EdgeInsets.only(top:70.0,left: 30,right: 30),
                child: Row(
                  //mainAxisAlignment: MainAxisAlignment.center,
                  
                  children: <Widget>[
                   /* Image(
                    image: AssetImage('assets/images/icono.png'),
                    height: 70.0,
                    width: 70.00,
                  ),*/
                    Text("DIÁLOGO \n MÉDICO - PACIENTE",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            /* shadows: [
                              Shadow(
                                blurRadius: 1.0,
                                color: Colors.blue,
                                offset: Offset(2.0, 2.0),
                              ),
                            ],*/
                            fontWeight: FontWeight.bold,
                            color: Theme.Colors.textInicio ,
                            fontSize: 31,
                            letterSpacing: 0.0
                            
                            )
                            )


                  ]),
               

                /*child:Text("LENGUAS DE BOLIVIA",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                             shadows: [
                              Shadow(
                                blurRadius: 20.0,
                                color: Colors.blue,
                                offset: Offset(2.0, 2.0),
                              ),
                            ],
                            fontWeight: FontWeight.bold,
                            color: Color(0xFF145a32),
                            fontSize: 35,
                            letterSpacing: 1.0
                            
                            )
                            )*/
                            
                ),
             ),
            Positioned(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
        
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  
                  children: <Widget>[
                    Card(
                      elevation: 4.0,
                      color: Colors.white,
                      margin: EdgeInsets.only(top:0,left: 20, right: 20),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15)),
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            /////////////  Email//////////////
            
                          Text("Correo Electrónico",
                       
                          textAlign: TextAlign.end,
                          
                          style: TextStyle(
                                        color: Colors.black,
                                        fontFamily: "Poppins-Medium",
                                        fontSize: 15,
                                        letterSpacing: 1.0)
                              ),
                    
                            TextField(
                              style: TextStyle(color: Color(0xFF000000)),
                              controller: mailController,
                              cursorColor: Color(0xFF9b9b9b),
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                prefixIcon: Icon(
                                  Icons.account_circle,
                                  color: Colors.grey,
                                ),
                                hintText: "Correo Electrónico",
                                hintStyle: TextStyle(
                                    color: Color(0xFF9b9b9b),
                                    fontSize: 15,
                                    fontWeight: FontWeight.normal),
                              ),
                            ),

                            /////////////// password////////////////////
                       
                         new Padding(padding: new EdgeInsets.all(10.0)),

                         Text("Contraseña",

                          textAlign: TextAlign.end,
                          style: TextStyle(
                            
                                        color: Colors.black,
                                        fontFamily: "Poppins-Medium",
                                        fontSize: 15,
                                        letterSpacing: 1.0)
                              ),
                         
                            TextField(
                              style: TextStyle(color: Color(0xFF000000)),
                              cursorColor: Color(0xFF9b9b9b),
                              controller: passwordController,
                              keyboardType: TextInputType.text,
                              obscureText: true,
                              decoration: InputDecoration(
                                prefixIcon: Icon(
                                  Icons.vpn_key,
                                  color: Colors.grey,
                                ),
                                hintText: "Contraseña",
                                hintStyle: TextStyle(
                                    color: Color(0xFF9b9b9b),
                                    fontSize: 15,
                                    fontWeight: FontWeight.normal),
                              ),
                            ),
                            /////////////  LogIn Botton///////////////////
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: FlatButton(
                                child: Padding(
                                  padding: EdgeInsets.only(
                                      top: 8, bottom: 8, left: 10, right: 10),
                                  child: Text(
                                    _isLoading? 'INICIANDO...' : 'INICIAR SESIÓN',
                                    textDirection: TextDirection.ltr,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: "Poppins-Bold",
                                        fontSize: 18,
                                        letterSpacing: 1.0)
                                  ),
                                ),
                                color: Theme.Colors.buttonIniciar,
                                disabledColor: Theme.Colors.buttonIniciando,
                                shape: new RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(20.0)),
                                onPressed: _isLoading ? null : _login,
                               
                              ),
                            ),
                ////////////   new account///////////////
                    Padding(
                      padding: const EdgeInsets.only(top: 0),
                      child: InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              new MaterialPageRoute(
                                  builder: (context) => SignUp()));
                        },
                        child: Text(
                          'Crear cuenta',
                          textDirection: TextDirection.ltr,
                          style: TextStyle(
                            color: Colors.blue,
                            fontSize: 17.0,
                            decoration: TextDecoration.none,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                      ),
                    ),
                          ],
                        ),
                      ),
                    ),
                           

                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _login() async{
    setState(() {
       _isLoading = true;
    });
/*
    Map<String, dynamic> jsonMap = {
      "credentials":{
        'email' : mailController.text, 
        'password' : passwordController.text}
    };
    String jsonString = json.encode(jsonMap); // encode map to json
*/
    print('======================================0');

    var data = {
      'credentials':{
        'email' : mailController.text, 
        'password' : passwordController.text}
    };
    var res = await CallApis().postData( data, 'auth');
    var body = json.decode(res.body);
        print(mailController.text);


print(body['error']);


print('xxxxxxxxxxxxxxxxxx');


    if(body['error']==null){
      //print(body['data']['token']);
      SharedPreferences localStorage = await SharedPreferences.getInstance();
      localStorage.setString('token', body['data']['token']);
      /*localStorage.setString('user', json.encode(body['data']['user']));
      print(body['data']);*/
     /* var ress = await CallApis().getData('users/self?include=person');
      var cuerpo = json.decode(ress.body);
      print(ress);
      print(cuerpo);*/
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => LanguagesList()));
    }else{
      _showMsg('Credenciales Incorrectas');
    }


    setState(() {
       _isLoading = false;
    });
  }
  
}

 //color: Theme.Colors.planetPageBackground,

/* ====================================================== */

class LoginCredentials {

  final String name;
  final String email;

  LoginCredentials(this.name, this.email);

  LoginCredentials.fromJson(Map<String, dynamic> json)
      : name = json['name'],
        email = json['email'];

  Map<String, dynamic> toJson() =>
    {
      'name': name,
      'email': email,
    };
 
}

class LoginData {

var user = new LoginCredentials('mayta5750@gmail.com', 'mayta5750');

  LoginData(LoginCredentials lc){
    this.user=lc;
  }

  Map<String, dynamic> toJson() =>
    {
      'credencials': user
    };
}

class LoginDatas {
    var credentials = new LoginCredentials('mayta5750@gmail.com', 'mayta5750');  
    LoginDatas(String email, String password) {
        credentials = new LoginCredentials(email, password);
    }
     LoginCredentials getCredentials() {
        return credentials;
    }

    void setCredentials(LoginCredentials credentials) {
        this.credentials = credentials;
    }
}
    class LoginCredentialss {
        String email;
        String password;

        LoginCredentialss(String email, String password) {
            this.email = email;
            this.password = password;
        }

        String getEmail() {
            return email;
        }

        void setEmail(String email) {
            this.email = email;
        }

        String getPassword() {
            return password;
        }

        void setPassword(String password) {
            this.password = password;
        } 
    }