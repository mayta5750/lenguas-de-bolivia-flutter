import 'dart:convert';

DictionaryWords tasksFromJson(String str) {
  final jsonData = json.decode(str);
  return DictionaryWords.fromMap(jsonData);
}

String tasksToJson(DictionaryWords data) {
  final dyn = data.toMap();
  return jsonEncode(dyn);
}

class DictionaryWords {
  String id_dictionary_word;
  String grapheme_target;
  String grapheme_source;

  DictionaryWords({this.id_dictionary_word, this.grapheme_target, this.grapheme_source});

  factory DictionaryWords.fromMap(Map<String, dynamic> json) => new DictionaryWords(
        id_dictionary_word: json["id_dictionary_word"].toString(),
        grapheme_target: json["grapheme_target"].toString(),
        grapheme_source: json["grapheme_source"].toString(),
      );

  Map<String, dynamic> toMap() => {
        "id_dictionary_word": id_dictionary_word,
        "grapheme_target": grapheme_target,
        "grapheme_source": grapheme_source,
      };
  String getIdDictionaryWord() {
    return id_dictionary_word;
  }

  void setIdDictionaryWord(String id_dictionary_word) {
    this.id_dictionary_word = id_dictionary_word;
  }
  String getGraphemeTarget() {
    return grapheme_target;
  }

  void setGraphemeTarget(String grapheme_target) {
    this.grapheme_target = grapheme_target;
  }

  String getGraphemeSource() {
    return grapheme_source;
  }

  void setGraphemeSource(String grapheme_source) {
    this.grapheme_source = grapheme_source;
  }
}