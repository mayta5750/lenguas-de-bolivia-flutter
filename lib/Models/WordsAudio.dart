import 'dart:convert';

WordsAudio tasksFromJson(String str) {
  final jsonData = json.decode(str);
  return WordsAudio.fromMap(jsonData);
}

String tasksToJson(WordsAudio data) {
  final dyn = data.toMap();
  return jsonEncode(dyn);
}

class WordsAudio {
  String id_dictionary_word;
  String path;

  WordsAudio({this.id_dictionary_word, this.path});

  factory WordsAudio.fromMap(Map<String, dynamic> json) => new WordsAudio(
        id_dictionary_word: json["id_dictionary_word"].toString(),
        path: json["path"].toString(),
      );

  Map<String, dynamic> toMap() => {
        "id_dictionary_word": id_dictionary_word,
        "path": path,
      };
  String getIdDictionaryWord() {
    return id_dictionary_word;
  }

  void setIdDictionaryWord(String id_dictionary_word) {
    this.id_dictionary_word = id_dictionary_word;
  }
  String getPath() {
    return path;
  }

  void setPath(String path) {
    this.path = path;
  }
}