import 'dart:convert';

WordsImage tasksFromJson(String str) {
  final jsonData = json.decode(str);
  return WordsImage.fromMap(jsonData);
}

String tasksToJson(WordsImage data) {
  final dyn = data.toMap();
  return jsonEncode(dyn);
}

class WordsImage {
  String id_dictionary_word;
  String path;

  WordsImage({this.id_dictionary_word, this.path});

  factory WordsImage.fromMap(Map<String, dynamic> json) => new WordsImage(
        id_dictionary_word: json["id_dictionary_word"].toString(),
        path: json["path"].toString(),
      );

  Map<String, dynamic> toMap() => {
        "id_dictionary_word": id_dictionary_word,
        "path": path,
      };
  String getIdDictionaryWord() {
    return id_dictionary_word;
  }

  void setIdDictionaryWord(String id_dictionary_word) {
    this.id_dictionary_word = id_dictionary_word;
  }
  String getPath() {
    return path;
  }

  void setPath(String path) {
    this.path = path;
  }
}