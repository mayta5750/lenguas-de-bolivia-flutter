import 'dart:convert';

Cards tasksFromJson(String str) {
  final jsonData = json.decode(str);
  return Cards.fromMap(jsonData);
}

String tasksToJson(Cards data) {
  final dyn = data.toMap();
  return jsonEncode(dyn);
}

class Cards {
  String id_lesson_card;
  String id_lesson;
  String cat_card_type;
  String id_key_item;
  String option_dict_word_1;
  String option_dict_word_2;
  String option_dict_word_3;
  Cards({this.id_lesson_card, this.id_lesson, this.cat_card_type,this.id_key_item, this.option_dict_word_1, this.option_dict_word_2, this.option_dict_word_3});
  factory Cards.fromMap(Map<String, dynamic> json) => new Cards(
        id_lesson_card: json["id_lesson_card"].toString(),
        id_lesson: json["id_lesson"].toString(),
        cat_card_type: json["cat_card_type"].toString(),
        id_key_item: json["id_key_item"].toString(),
        option_dict_word_1: json["option_dict_word_1"].toString(),
        option_dict_word_2: json["option_dict_word_2"].toString(),
        option_dict_word_3: json["option_dict_word_3"].toString(),
      );

  Map<String, dynamic> toMap() => {
        "id_lesson_card": id_lesson_card,
        "id_lesson": id_lesson,
        "cat_card_type": cat_card_type,
        "id_key_item": id_key_item,
        "option_dict_word_1":option_dict_word_1,
        "option_dict_word_2":option_dict_word_2,
        "option_dict_word_3":option_dict_word_3,
      };
  String getIdLessonCard() {
    return id_lesson_card;
  }

  void setIdLessonCard(String id_lesson_card) {
    this.id_lesson_card = id_lesson_card;
  }

  String getIdLesson() {
    return id_lesson;
  }

  void setIdLesson(String id_lesson) {
    this.id_lesson = id_lesson;
  }

  String getCatCardType() {
    return cat_card_type;
  }

  void setCatCardType(String cat_card_type) {
    this.cat_card_type = cat_card_type;
  }

  String getIdKeyItem() {
    return id_key_item;
  }

  void setIdKeyItem(String id_key_item) {
    this.id_key_item = id_key_item;
  }

  String getOptionDictWord_1() {
    return option_dict_word_1;
  }

  void setOptionDictWord_1(String option_dict_word_1) {
    this.option_dict_word_1 = option_dict_word_1;
  }

  String getOptionDictWord_2() {
    return option_dict_word_2;
  }

  void setOptionDictWord_2(String option_dict_word_2) {
    this.option_dict_word_2 = option_dict_word_2;
  }

  String getOptionDictWord_3() {
    return option_dict_word_3;
  }

  void setOptionDictWord_3(String option_dict_word_3) {
    this.option_dict_word_3 = option_dict_word_3;
  }
}