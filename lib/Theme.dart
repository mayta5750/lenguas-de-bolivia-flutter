import 'package:flutter/material.dart';

class Colors {

  const Colors();


  static const Color appBarTitle = const Color(0xFFFFFFFF);
  static const Color appBarIconColor = const Color(0xFFFFFFFF);
  static const Color appBarDetailBackground = const Color(0x00FFFFFF);
  static const Color appBarGradientStart = const Color(0xFF3383FC);
  static const Color appBarGradientEnd = const Color(0xFF00C6FF);

  //static const Color planetCard = const Color(0xFF434273);
  static const Color planetCard = const Color(0xFF8685E5);
  //static const Color planetListBackground = const Color(0xFF3E3963);
  static const Color planetPageBackground = const Color(0xFF736AB7);
  static const Color planetTitle = const Color(0xFFFFFFFF);
  static const Color planetLocation = const Color(0x66FFFFFF);
  static const Color planetDistance = const Color(0x66FFFFFF);


//color de app Bar
  static const Color barBackground = const Color(0xFF39B0E1);
  static const Color textInicio = const Color(0xFF0099D9);


//color de app Bar
  static const Color buttonIniciar= const Color(0xFF1CA4DD);
  static const Color buttonIniciando = const Color(0xFF1978b9);

//banner languages
  static const Color banerLanguages = const Color(0xFFffffff);

  //Colores de textos lecciones negro
    static const Color textLec = const Color(0xFF5d6d7e);
    //plomo
        static const Color texSubtLec = const Color(0XFF808b96);

// unidad
  static const Color unidadBackground = const Color(0xFF88D0ED);
  static const Color containerBackground = const Color(0xFFE3F4FB);

  static const Color unidadText = const Color(0xFFffffff);

  static const Color themeText = const Color(0xFF77C8EB);

    static const Color background = const Color(0xFFFFFFFF);

//theme
  static const Color carruselBackgroundLec = const Color(0XFFE3F4FB);
  static const Color progresLec = const Color(0XFF30e218);


//color body de toda las pantallas
        
        static const Color bodyBackground = const Color(0XFFF2F3F4);
//search

        static const Color searchIcon = const Color(0XFF88D0ED);
        static const Color searchCard = const Color(0XFFE3F4FB);


//color carrusel

        static const Color carruselBackground = const Color(0XFFFfffff);
        //COLOR boton !comenzar! leccion
static const Color buttonComezar = const Color(0XFF28B463);

//progres bas UNIDAD TEMA
static const Color barTheme = const Color(0XFF28B463);
/* CARDS */

static const Color textSource = const Color(0XFF39B0E1);
static const Color textTarget = const Color(0XFF28B463);
static const Color textOption = const Color(0XFF223345);


/* buttons */
static const Color collectionButtons = const Color(0XFF88D0ED);
static const Color collectionButtonsPressed = const Color(0XFFf96608 );

static const Color buttonNext = const Color(0XFF77C8EB);


static const Color showMsg = const Color(0XFFf96608);

static const Color showMsgInCorrecto = const Color(0xFFf91308);
static const Color showMsgCorrecto = const Color(0xFFaaf255);
static const Color loadCard = const Color(0x99d4d7d4);

static const Color bodyCard_1 = const Color(0xffebf1eb);
static const Color bodyCard_2 = const Color(0xfffbfbfb);

static const Color colorDuo = const Color(0XFF28B463);


}

class Dimens {
  const Dimens();

  static const planetWidth = 100.0;
  static const planetHeight = 100.0;
}

class TextStyles {

  const TextStyles();

  static const TextStyle appBarTitle = const TextStyle(
    color: Colors.appBarTitle,
    fontFamily: 'Poppins',
    fontWeight: FontWeight.w600,
    fontSize: 36.0
  );

  static const TextStyle planetTitle = const TextStyle(
    color: Colors.planetTitle,
    fontFamily: 'Poppins',
    fontWeight: FontWeight.w600,
    fontSize: 24.0
  );

  static const TextStyle planetLocation = const TextStyle(
    color: Colors.planetLocation,
    fontFamily: 'Poppins',
    fontWeight: FontWeight.w300,
    fontSize: 14.0
  );

  static const TextStyle planetDistance = const TextStyle(
    color: Colors.planetDistance,
    fontFamily: 'Poppins',
    fontWeight: FontWeight.w300,
    fontSize: 12.0
  );


}