import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:tutorial_project/Theme.dart' as Theme;
import 'dart:convert';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fake_okhttp/fake_okhttp.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tutorial_project/api/data.dart';
import 'package:tutorial_project/api/apis.dart';

import 'package:tutorial_project/Selection/selectionListScreen.dart';

import 'package:shared_preferences/shared_preferences.dart';


class LanguagesList extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
       // primarySwatch: Colors.appBarTitle,
      ),
      home: MyLanguagesListPage(title: 'Tus Cursos'),
    );
  }
}

class MyLanguagesListPage extends StatefulWidget {
  MyLanguagesListPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyLanguagesListPageState createState() => _MyLanguagesListPageState();
}

class _MyLanguagesListPageState extends State<MyLanguagesListPage> {

  List data;

  // Function to get the JSON data
  Future<String> getJSONData() async {

    var response = await DataApi().getLanguageList();
    var res = await CallApis().getData('users/self?include=person');

    setState(() {
      // Get the JSON data
      data = json.decode(response.body)['data'];
      var cuerpo = json.decode(res.body);
      print(res);
      print(cuerpo);
    });
    return "Successfull";
  }
  @override
  Widget build(BuildContext context) { 

    return Scaffold(
          
      appBar: AppBar(
        title: Text(widget.title),
        backgroundColor: Theme.Colors.barBackground,
      ),
      backgroundColor: Theme.Colors.bodyBackground,
      body: CustomScrollView(
          slivers: <Widget>[
            // Agrega un app bar al CustomScrollView
            SliverAppBar(
              backgroundColor: Theme.Colors.banerLanguages,
              // Provee un título estándard
              title: Text('OEI'),
              // Permite al usuario revelar el app bar si comienza a desplazarse
              // hacia arriba en la lista de elementos
              floating: true,              
              // Mostrar un widget placeholder para visualizar el tamaño de reducción
              flexibleSpace: FlexibleSpaceBar(
                  centerTitle: true,
                  title: Text("",
                  //title: Text("Elige un curso y \n continúa aprendiendo",
                    textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16.0,
                      )),
                  background: 
                  Image(
                    image: AssetImage('assets/ops/banner.jpg'),
                    width: 100,
                    height: 50,
                  ),
                  /*Image.network(
                    "https://images.pexels.com/photos/396547/pexels-photo-396547.jpeg?auto=compress&cs=tinysrgb&h=350",
                    fit: BoxFit.cover,
                  )*/
                  ),
              // Aumentar la altura inicial de la SliverAppBar más de lo normal
              expandedHeight: 225,
            ),
            // A continuación, crea un SliverList
            SliverList(
              // Para ello, se utiliza un delegado para crear elementos a medida que
              // se desplazan por la pantalla. 
              
              delegate: SliverChildBuilderDelegate(
                // La función builder devuelve un ListTile con un título que
                // muestra el índice del elemento actual
                (context, index) => _buildImageColumn(data[index]),
                // Construye 1000 ListTiles
                childCount:data == null ? 0 : data.length,
              ),
            ),
          ],
        ),
       // _buildListView(),       
    );
  }

  Widget _buildListView() {
  
    return ListView.builder(
      
      
    
      padding: const EdgeInsets.all(16.0),
      itemCount: data == null ? 0 : data.length,
      itemBuilder: (context, index) {
        
        return _buildImageColumn(data[index]);
        // return _buildRow(data[index]);
      }
    );
  }

  Widget _buildImageColumn(dynamic item) => Container(
    
      decoration: BoxDecoration(
        color: Colors.white54
      ),
      margin: const EdgeInsets.all(10),
      
      
       child: Card(
        child:Row(
          children: <Widget>[
          
            new ClipRRect(
              borderRadius: BorderRadius.circular(9.0),
              child:item['path'].substring(item['path'].length-3)=='svg' ?               
                   new SvgPicture.network(
                    item['path'],
                    semanticsLabel: 'Feed button',
                    height: 110.0,
                    width: 110.00,
                    fit: BoxFit.contain,
                   // placeholderBuilder: (context,) => Icon(Icons.error),
                    ):
                    new CachedNetworkImage(                
                    imageUrl: item['path'],
                   // placeholder: (context, url) => new CircularProgressIndicator(),
                    height: 110.0,
                    width: 110.00,
                    fit: BoxFit.contain,
                   // errorWidget: (context, url, error) => new Icon(Icons.error),
                    fadeOutDuration: new Duration(seconds: 1),
                    fadeInDuration: new Duration(seconds: 3),
                  ),
            ),
            Expanded(
                child:GestureDetector(
                  onTap: () {
                    _onSelection(item);
                    //print("presion");
                  },
                  child:_buildRow(item)
                ),
            ),
              const Icon(
              Icons.more_vert,
              size: 25.0,
            ),
          ],
        )
      ),
    );
  Widget _buildRow(dynamic item) {
    return ListTile(
      title: Text(
        item['detail'] == null ? '': item['detail'],
        //"Diálogos de Salud",
        style: TextStyle(
            //color: Colors.white,
            fontWeight: FontWeight.bold,
            fontFamily: "Poppins-Bold",
            fontSize: 22,
            letterSpacing: 2.0,
            wordSpacing: 1.0,
            )
      ),
      subtitle: Text("" + item['language'],
      style: TextStyle(
            //color: Colors.white,
        fontWeight: FontWeight.bold,
        wordSpacing: 10.0,
        fontSize: 20,
        letterSpacing: 1.0)
      ),
      
    );
  }
  @override
  void initState() {
    super.initState();
    // Call the getJSONData() method when the app initializes
    this.getJSONData();
  }
  void _onSelection(dynamic item)async{
    //print(item);
      SharedPreferences localStorage = await SharedPreferences.getInstance();
      //localStorage.setString('languageList', json.encode(item));
      localStorage.setString('id_version', item['id_version'].toString());
      localStorage.setString('language', item['language'].toString());
        //print(item['language']);
        //Navigator.of(context).pushNamed('/unidad');
     /* Navigator.push(
        context,
        MaterialPageRoute(
        builder: (context) => UnidadList())); */
      Navigator.push(
        context,
        MaterialPageRoute(
        builder: (context) => SelectionList(id_version:item['id_version'].toString(),idioma:item['language'])));  
  }
  
}