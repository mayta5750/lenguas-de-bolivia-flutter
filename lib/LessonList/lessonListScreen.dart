import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:tutorial_project/Cards/CardsScreen.dart';
import 'package:tutorial_project/api/data.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:tutorial_project/Theme.dart' as Theme;
import 'package:percent_indicator/percent_indicator.dart';

class LessonList extends StatelessWidget {
  LessonList({Key key, this.id_theme ,this.icon_url ,this.description ,this.detail}) : super(key: key);
  final String id_theme;
  final String icon_url;
  final String description;
  final String detail;
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(
        title: Text('Lecciones'),
        backgroundColor: Theme.Colors.barBackground,
        leading: IconButton(icon:Icon(Icons.arrow_back),
        //color: Colors.black,
        onPressed:() => Navigator.pop(context)),
      ),
      body: MyLessonListPage(
        title: 'Lecciones',
        id_theme: this.id_theme,
        icon_url: this.icon_url,
        description: this.description,
        detail: this.detail
      ),
    );
  }
}

class MyLessonListPage extends StatefulWidget {
  MyLessonListPage({Key key, this.title,this.id_theme ,this.icon_url ,this.description ,this.detail}) : super(key: key);
  final String title;
  final String id_theme;
  final String icon_url;
  final String description;
  final String detail;
  @override
  _MyLessonListPageState createState() => _MyLessonListPageState();
}
class _MyLessonListPageState extends State<MyLessonListPage> with SingleTickerProviderStateMixin {
  List data =[];
  List scoreLesson =[];
  bool _isLoading = false;

   Animation<double> animation;
  AnimationController controller;
  // Function to get the JSON data
  Future<String> getJSONData() async {
    var response = await DataApi().getLessonList(widget.id_theme.toString());
    var getScoreLesson = await DataApi().getScoreLesson(widget.id_theme.toString());
    setState(() {
      // Get the JSON data
      data = json.decode(response.body)['data'];
      scoreLesson = json.decode(getScoreLesson.body)['data'];
    });
    //print (scoreLesson);
    return "Successfull";
  }
  @override
  Widget build(BuildContext context) { 
    return Scaffold(
      backgroundColor: Theme.Colors.bodyBackground,
      body:Container(
        color: Theme.Colors.background,
        child:_buildContent(),        

      )
      //_ultima(), para tarjetas
      //_pag(),// para tarjetas
    );
  }
   Widget _buildContent() {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          _themeAvatar(),
          _themeText(),       
          _carrousel(),
        ],
      ),
    );
  }

_buscarScore(int id_lesson){
  if(id_lesson!=1000000200){
    for (var i = 0; i < scoreLesson.length; i++) {
      if(id_lesson==scoreLesson[i]['id_lesson']){
        double score=double.parse(scoreLesson[i]['score'])*100;
        return score;
      }
    }
  }
  return 0.0;
}










  Widget _ultima(){
    return Container(
        margin: EdgeInsets.symmetric(
          vertical: 140.0,
        ),
        child: PageView(
          controller: PageController(
            initialPage: 1,
            viewportFraction: 0.8,
          ),
          children: [
            Container(margin: EdgeInsets.symmetric(horizontal: 10.0,), color: Colors.redAccent),
            Container(margin: EdgeInsets.symmetric(horizontal: 10.0,), color: Colors.purpleAccent),
            Container(margin: EdgeInsets.symmetric(horizontal: 10.0,), color: Colors.greenAccent)
          ],
        ),
      );
  }
    Widget _pag(){
      return PageView(
      children: <Widget>[
        Container(
          color: Colors.pink,
        ),
        Container(
          color: Colors.cyan,
        ),
        Container(
          color: Colors.deepPurple,
        ),
      ],
    );
  }
  Widget _buildLessonItem(dynamic item) {   
    double score = _buscarScore(item['id_lesson']);
    //_buscarScore(1);   
   // print(item['id_lesson'].toString()); 
    return  Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Image(
              image: AssetImage('assets/icons/Lesson.png'),
              width: 80,
              height: 80,
              fit: BoxFit.cover
          ),
          new Padding(padding: new EdgeInsets.all(5.0))
          ,
          Text(item['description'] == null ? '': item['description'],
            style: TextStyle(color: Theme.Colors.textLec,
            fontWeight: FontWeight.w700,
            fontSize: 20.0)
          ), 
           Padding(
              padding: EdgeInsets.only(top:15.0,left: 15.0, right: 15.0,bottom: 15.0),
              child: new LinearPercentIndicator(
                animation: true,
                animationDuration: 1000,
                lineHeight: 20.0,
                percent: score/100,
                center: Text(score.toStringAsFixed(1)+" %"),
                linearStrokeCap: LinearStrokeCap.butt,
                progressColor: Theme.Colors.progresLec,
              ),
            ),
            _eventButton(
              item['id_lesson']== null ? '': item['id_lesson'].toString(),
              item['description']== null ? '': item['description'].toString()
            ),
        ],  
    );
  }
    Widget _themeText() {
    return Padding(
      padding: const EdgeInsets.only(top: 16.0, left: 16.0, right: 16.0),
      child: Column(
        //crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            widget.description,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Theme.Colors.textLec,
              fontWeight: FontWeight.bold,
              fontSize: 20.0,
              letterSpacing: 4,
              
            ),
          ),
          Padding(padding: const EdgeInsets.all(4.0),),
          Text(
            widget.detail,
            style: TextStyle(
              color: Theme.Colors.texSubtLec,
              fontSize: 20.0,
              //color: Colors.white.withOpacity(0.85),
              fontWeight: FontWeight.w500,
            ),
          ),
          Container(
            color: Colors.transparent,
            margin: const EdgeInsets.symmetric(vertical: 16.0),
            width: 225.0,
            height: 1.0,
          ),
          /*Text(
            "",
            style: TextStyle(
              color: Colors.black,
              //color: Colors.white.withOpacity(0.85),
              height: 1.4,
            ),
          ),*/
        ],
      ),
    );
  }
    Widget _themeAvatar() {
    return Container(
      width: 110.0,
      height: 110.0,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(color: Colors.white30),
      ),
      margin: const EdgeInsets.only(top: 35.0),
      //padding: const EdgeInsets.all(2.0),
      child: ClipOval(
        child:widget.icon_url==null? 
          Image(
              image: AssetImage('assets/icons/theme.png'),
              fit: BoxFit.cover
          )
        :
          Image.network(
            widget.icon_url,
            fit: BoxFit.cover,
          )
      ),
    );
  }
  Widget _eventButton(String idLesson, String description){
    return 
     // padding: const EdgeInsets.all(0.0),
       FlatButton(
      child: Padding(
      padding: EdgeInsets.only(
      top: 8, bottom: 8, left: 10, right: 10),
      child: Text(
      _isLoading? 'INICIANDO...' : '¡COMENZAR!',
      //textDirection: TextDirection.ltr,
      style: TextStyle(
      color: Colors.white,
      fontFamily: "Poppins-Bold",
      fontSize: 15,
      letterSpacing: 1.0)
      ),
      ),
      color: Theme.Colors.buttonIniciar,
      disabledColor: Theme.Colors.buttonIniciando,
      shape: new RoundedRectangleBorder(
      borderRadius:
      new BorderRadius.circular(20.0)),
      onPressed:(){
        setState(() {_isLoading = true;});
          Navigator.push(
            context,
            new MaterialPageRoute(
            builder: (context) => Cards(idLesson: idLesson,description:description)));  
        setState(() {_isLoading = false;});
      }
    );
  }
  Widget _carrousel() {
    if((data.length==0)){
        return new Swiper(
         // layout: SwiperLayout.CUSTOM,
          customLayoutOption: new CustomLayoutOption(
              startIndex: -1,
              stateCount: 3
          ).addRotate([
            -45.0/180,
            0.0,
            45.0/180
          ]).addTranslate([
            new Offset(-370.0, -40.0),
            new Offset(0.0, 0.0),
            new Offset(370.0, -40.0)
          ]),
          itemWidth: 300.0,
          itemHeight: 250.0,
          itemBuilder: (BuildContext context, int index) {
            return new Card(
              color: Theme.Colors.carruselBackground,
              child: Padding(
              padding: const EdgeInsets.all(30.0),
              child: new Center(
                child: Container(
                        //child:_buildLessonItem(data[index]),
                      ),
              ),
              ),
            );
          },
          itemCount: 1,
          //pagination: new SwiperPagination(alignment: Alignment.bottomCenter),
          );
    }else{
        return new Swiper(
          layout: SwiperLayout.CUSTOM,
          customLayoutOption: new CustomLayoutOption(
              startIndex: -1,
              stateCount: 3
          ).addRotate([
            -45.0/180,
            0.0,
            45.0/180
          ]).addTranslate([
            new Offset(-370.0, -40.0),
            new Offset(0.0, 0.0),
            new Offset(370.0, -40.0)
          ]),
          itemWidth: 300.0,
          itemHeight: 280.0,
          itemBuilder: (BuildContext context, int index) {
            
            return new Card(
              elevation: 10,
              color: Theme.Colors.carruselBackgroundLec,
              child:Padding(
              padding: const EdgeInsets.all(30.0),
                child: new Center(
                  child: Container(
                          child:_buildLessonItem(data[index]),
                        ),
                ),
              )
            );
          },
          itemCount: (data == null ? 0 : data.length),
          //key: UniqueKey(),
          scrollDirection: Axis.horizontal,
          //pagination: new SwiperPagination(),
          pagination: new SwiperPagination(
                margin: new EdgeInsets.fromLTRB(0.0, 240.0, 0.0, 30.0),
                builder: new DotSwiperPaginationBuilder(
                    color: Colors.black12,
                    activeColor: Colors.blue,
                    size: 20.0,
                    activeSize: 20.0)),
            
        //control: new SwiperControl(),
        );
    }

  }
    void onCards() async{
    setState(() {
       _isLoading = true;
    });
     /* Navigator.push(
        context,
        new MaterialPageRoute(
        builder: (context) => UnidadList())); */  
            setState(() {
       _isLoading = false;
    });
  }
  @override
  void initState() {
    super.initState();
    // Call the getJSONData() method when the app initializes
    this.getJSONData();
  }


}