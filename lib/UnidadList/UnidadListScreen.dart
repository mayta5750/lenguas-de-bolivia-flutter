import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:tutorial_project/api/data.dart';
import 'package:tutorial_project/LessonList/lessonListScreen.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:tutorial_project/Theme.dart' as Theme;
import 'package:percent_indicator/percent_indicator.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UnidadList extends StatelessWidget {
  UnidadList({Key key, this.idioma, this.id_version}) : super(key: key);
  final String idioma;
  final String id_version;
  @override
  Widget build(BuildContext context) {
    print(id_version);
    return Scaffold(
      appBar: AppBar(
        title: Text('Unidades'),
        backgroundColor: Theme.Colors.barBackground,
        leading: IconButton(icon:Icon(Icons.arrow_back),
        //color: Colors.black,
        onPressed:() => Navigator.pop(context)),
      ),
      body: MyUnidadListPage(title: 'Unidades',),
    );
  }
}
class MyUnidadListPage extends StatefulWidget {
  MyUnidadListPage({Key key, this.title, this.id_version,this.idioma}) : super(key: key);

  final String title;
  final String idioma;
  final String id_version;

  @override
  _MyUnidadListPageState createState() => _MyUnidadListPageState();
}

class _MyUnidadListPageState extends State<MyUnidadListPage> {

  List data;

  // Function to get the JSON data
  Future<String> getJSONData() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
   // var id_version = widget.id_version.toString(); 
    var version = localStorage.getString('id_version'); 
    var response = await DataApi().getUnidadList(version);
    setState(() {
      // Get the JSON data
      data = json.decode(response.body)['data'];
      /*var dat = {
        'description':"Pronto Nuevos Temas",
      'themes':{
        'description' : "", 
        'icon_url' : ""}
    };*/
     // data.add(dat);
      print(data);
    });
    return "Successfull";
  }
  @override
  Widget build(BuildContext context) { 
    return Scaffold(
      backgroundColor: Theme.Colors.bodyBackground,
      body:_buildListView(),
    );
  }
/* ====================== Lista de UNIDADES y TEMAS ========================*/
  Widget _buildListView() {
    return Container(
  // padding: const EdgeInsets.only(top:16.0,bottom: 16.0,right: 7,left: 7),
    child:ListView.separated(       
      //physics: const NeverScrollableScrollPhysics(),
          padding: const EdgeInsets.all(16.0),
          itemBuilder: (context, index) {
            return _buildListUnidadView(data[index]);
          },
          separatorBuilder: (context, index) {
            return Column(
              children: <Widget>[
            _buildListThemeView(data[index]),
            Container(
              color: Color(0x00000000),
              padding: const EdgeInsets.all(10.0),
            ),
            ],
            );
           // return _carrousel(data[index]);
          },
         itemCount: data == null ? 0 : data.length,
    )
    );
  }
  /* ====================== Lista de UNIDADES y TEMAS ========================*/
  /*Widget _buildImageColumn(dynamic item) => Container(  
        decoration: BoxDecoration(
        color: Colors.white54
      ),
      margin: const EdgeInsets.all(4),
      child: Column(
        children: [
          GestureDetector(
            onTap: () {
              _onUnidad();
            },
          child:_buildRow(item)
          ),
        ],
      ),      
    );*/
 /* ====================== Lista de UNIDADES ========================*/

  Widget _buildListUnidadView(dynamic item) {  
    return Container(
    color: Theme.Colors.unidadBackground,
    padding: const EdgeInsets.only(top:16.0,bottom: 16.0,right: 7,left: 7),

    child:Text(
      item['description'] == null ? '': item['description'],
      textAlign: TextAlign.center,
      
      style: TextStyle(
       /* shadows: [
          Shadow(
            blurRadius: 15.0,
            color: Colors.blue,
            offset: Offset(2.0, 2.0),
          ),
        ],*/
        fontWeight: FontWeight.bold,
        color: Theme.Colors.unidadText,
        fontSize: 20,
        letterSpacing: 1.0 
      )
      ),
    );
  }
/* ====================== Lista de TEMAS ANIDADAS========================*/
  Widget _buildListThemeView(dynamic item) {
    //int pos = int.parse(item['position']);
    int pos=3;
    int i=-1;
    return 
    Card(
      color: Theme.Colors.containerBackground,
      elevation: 3,
      child:new ListView.builder(
                    //  crossAxisSpacing: 10.0,
          physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        padding: const EdgeInsets.all(0.0),
        itemCount: item['themes']['data'] == null ? 0 : (item['themes']['data'].length),
      //  gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: pos),
        itemBuilder: (BuildContext context, int index) {
        /*  if(index%2==0)
              return Container();
          else{
              i++;*/
              return _buildColumnTheme(item['themes']['data'][index]);
         // }
        }
      )
    );
  }
  Widget _buildColumnTheme(dynamic item) => Container(
      decoration: BoxDecoration(
        color: Colors.transparent
      ),
      margin: const EdgeInsets.only(top:15,right: 40, left: 40, bottom: 20),
      child: Column(
        children: [
          GestureDetector(
            onTap: () {
              _onTheme(item);
            },
          child:new CircularPercentIndicator(
                radius: 70,
               // lineWidth: 13.0,
                animation: true,
                percent: 1.0,
              //  center:_builDescreptionTheme(item),
                center: _builImagenTheme(item),
                circularStrokeCap: CircularStrokeCap.round,
                progressColor: Theme.Colors.barTheme,
              ),
            ), 
          _builDescreptionTheme(item),
        ],
      ),
    );
  Widget _builDescreptionTheme(dynamic item) {  
    return Text(
              item['description'] == null ? '': item['description'],
              textAlign: TextAlign.center,
              style: TextStyle( 
                             /*shadows: [
                              Shadow(
                                blurRadius: 15.0,
                                color: Colors.blue,
                                offset: Offset(2.0, 2.0),
                              ),
                            ],*/
                            fontWeight: FontWeight.bold,
                            color: Theme.Colors.themeText,
                            fontSize: 18,
                            letterSpacing: 1.0
                            
              ),
    );
  }
  Widget _builImagenTheme(dynamic item) {  
    return 
    new CachedNetworkImage(
            imageUrl: item['icon_url'] == null ? 'https://back.tecnicos.lenguas.org.bo/resources/themes5b69cf29c34687.33406949.png': item['icon_url'],
            placeholder: (context, url) => new CircularProgressIndicator(),
            errorWidget: (context, url, error) => new Icon(Icons.error),
            fadeOutDuration: new Duration(seconds: 1),
            fadeInDuration: new Duration(seconds: 3),
          );
  }

/* =============================================================== */
  Widget _carrousel(dynamic item) {
    if((data.length==0)){
        return new Swiper(
         // layout: SwiperLayout.CUSTOM,
          customLayoutOption: new CustomLayoutOption(
              startIndex: -1,
              stateCount: 3
          ).addRotate([
            -45.0/180,
            0.0,
            45.0/180
          ]).addTranslate([
            new Offset(-370.0, -40.0),
            new Offset(0.0, 0.0),
            new Offset(370.0, -40.0)
          ]),
          itemWidth: 300.0,
          itemHeight: 250.0,
          itemBuilder: (BuildContext context, int index) {
            return new Card(
              color: Theme.Colors.carruselBackground,
              child: Padding(
              padding: const EdgeInsets.all(30.0),
              child: new Center(
                child: Container(
                        //child:_buildLessonItem(data[index]),
                      ),
              ),
              ),
            );
          },
          itemCount: 1,
          //pagination: new SwiperPagination(alignment: Alignment.bottomCenter),
          );
    }else{
        return new Swiper(
          layout: SwiperLayout.CUSTOM,
          customLayoutOption: new CustomLayoutOption(
              startIndex: -1,
              stateCount: 3
          ).addRotate([
            -45.0/180,
            0.0,
            45.0/180
          ]).addTranslate([
            new Offset(-370.0, -40.0),
            new Offset(0.0, 0.0),
            new Offset(370.0, -40.0)
          ]),
          itemWidth: 300.0,
          itemHeight: 200.0,
          itemBuilder: (BuildContext context, int index) {
            
            return new Card(
              color: Theme.Colors.carruselBackground,
              child:Padding(
              padding: const EdgeInsets.all(30.0),
                child: new Center(
                  child: Container(
                          child:_buildColumnTheme(item['themes']['data'][index]),
                        ),
                ),
              )
            );
          },
          itemCount: item['themes']['data'] == null ? 0 : (item['themes']['data'].length),
          //key: UniqueKey(),
          scrollDirection: Axis.horizontal,
          //pagination: new SwiperPagination(),
          pagination: new SwiperPagination(
                margin: new EdgeInsets.fromLTRB(0.0, 250.0, 0.0, 30.0),
                builder: new DotSwiperPaginationBuilder(
                    color: Colors.black12,
                    activeColor: Colors.blue,
                    size: 20.0,
                    activeSize: 20.0)),
            
        //control: new SwiperControl(),
        );
    }

  }



  @override
  void initState() {
    super.initState();
    // Call the getJSONData() method when the app initializes
    this.getJSONData();
  }
  void _onTheme(dynamic item)async{
      //SharedPreferences localStorage = await SharedPreferences.getInstance();
      //localStorage.setString('id_theme', item['id_theme'].toString());
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => LessonList(
              id_theme: item['id_theme'].toString(),
              icon_url:item['icon_url'],
              description:item['description'],
              detail:item['detail'],
              ),
            ),
          );
            
  }
}