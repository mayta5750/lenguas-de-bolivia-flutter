import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'package:tutorial_project/Models/WordsImage.dart';
import 'package:tutorial_project/Models/WordsAudio.dart';
import 'package:tutorial_project/Models/DictionaryWords.dart';

import 'package:tutorial_project/Models/Cards.dart';

import 'package:tutorial_project/db/database.dart';
import 'package:tutorial_project/api/apis.dart';

import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqlite_api.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DataApi {

  getLanguageList() async {
    return CallApis().getLanguageList();
  }

  getUnidadList(version) async {
    return CallApis().getUnidadList(version);
  }

  getLessonList(id_theme) async {
    return CallApis().getLessonList(id_theme);
  }

  getScoreLesson(id_theme) async {
    return CallApis().getScoreLesson(id_theme);
  }

  getSearchList() async {
    return CallApis().getSearchList();
  }

  getCards(idLesson) async {
    print('===================A======================');
    if (await DBProvider.bd.getCards(idLesson) != null) {
      print('===================B======================');
      return await DBProvider.bd.getCards(idLesson);
    } else {
      print('===================C======================');
      var cards = CallApis().getCards(idLesson);
      /*print(cards);
      var aux = cards.isEmpty?0:cards.length;
      print(aux);*/
  
    print('cards');
  
      //if (cards.statusCode == 200) {
       // cards = json.decode(cards.body)['data'];
        
        /*for (int i = 0; i < cards.length; i++) {
          await DBProvider.bd.newCards(new Cards(
              id_lesson_card: cards[i]["id_lesson_card"].toString(),
              id_lesson: cards[i]["id_lesson"].toString(),
              cat_card_type: cards[i]["cat_card_type"].toString(),
              id_key_item: cards[i]["id_key_item"].toString()));
        }*/
   // }

      return cards;
    }
  }

  getWordImage(wordId) async {
    if (await DBProvider.bd.getWordsImage(wordId) != null) {
      print('============== IMAGEN =============== DB------');
      return await DBProvider.bd.getWordsImage(wordId);
    } else {
      print('============== IMAGEN =============== API9');
      return CallApis().getWordImage(wordId);
      //return await DBProvider.bd.getWordsImage(wordId);
    }
  }

  getWordAudio(wordId) async {
    if (await DBProvider.bd.getWordsAudio(wordId) != null) {
      print('============ AUDIO =============== DB------');
     // print(await DBProvider.bd.getWordsAudio(wordId));
      return await DBProvider.bd.getWordsAudio(wordId);
    } else {
      print('============== AUDIO =============== API9');
     return await CallApis().getWordAudio(wordId);
     // return await DBProvider.bd.getWordsAudio(wordId);
     // return response;
    }
  }

  getDictionaryWords(wordId) async {
    if (await DBProvider.bd.getDictionaryWords(wordId) != null) {
      print('============== Dictionary Words ============= DB------');
      return await DBProvider.bd.getDictionaryWords(wordId);
    } else {
      print('============== Dictionary Words ============= API9');
     return  CallApis().getDictionaryWords(wordId);
     // return await DBProvider.bd.getDictionaryWords(wordId);

    }
  }
}
