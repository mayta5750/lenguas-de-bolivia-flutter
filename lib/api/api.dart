import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'package:tutorial_project/Models/WordsImage.dart';
import 'package:tutorial_project/Models/WordsAudio.dart';
import 'package:tutorial_project/Models/DictionaryWords.dart';

import 'package:tutorial_project/Models/Cards.dart';

import 'package:tutorial_project/db/database.dart';

import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqlite_api.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class CallApi {
  //  final String _url = 'http://logintut.localhost/api/';
   final String _url = 'https://back.tecnicos.lenguas.org.bo/api/v1/';
//  final String _url = 'https://back.lenguas.oei.bo/api/v1/';
  postData(data, apiUrl) async {
    var fullUrl = _url + apiUrl + await _getToken();
    return await http.post(fullUrl,
        body: jsonEncode(data), headers: _setHeaders());
  }

  getData(apiUrl) async {
    var fullUrl = _url + apiUrl + await _getToken();
    return await http.get(fullUrl, headers: _setHeaders());
  }

  _setHeaders() => {
        'OEI-Token': 'app',
        'Content-type': 'application/json',
        'Accept': 'application/json',
      };

  _getToken() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var token = localStorage.getString('token');
    return '?token=$token';
  }

  getLanguageList() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var token = localStorage.getString('token');
    var response = await http.get(
        Uri.encodeFull(_url + "client/user/enrolledCourses?includeBeta=true"),
        headers: {
          "Accept": "application/json",
          "Authorization": "Bearer " + token
        });
    return response;
  }

  getUnidadList(version) async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var token = localStorage.getString('token');
    var response = await http.get(
        Uri.encodeFull(
            _url + "client/user/levels/" + version + "?include=themes"),
        headers: {
          "Accept": "application/json",
          "Authorization": "Bearer " + token
        });
    return response;
  }

  getLessonList(id_theme) async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var token = localStorage.getString('token');
    var response = await http.get(
        Uri.encodeFull(_url + "client/user/lessons/" + id_theme + "/"),
        headers: {
          "Accept": "application/json",
          "Authorization": "Bearer " + token
        });
    return response;
  }

  //obtiene el score por leccion
  getScoreLesson(id_theme) async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var token = localStorage.getString('token');
    var response = await http.get(
        Uri.encodeFull(_url + "client/user/lessonsDetail/" + id_theme + "/"),
        headers: {
          "Accept": "application/json",
          "Authorization": "Bearer " + token
        });
    return response;
  }

  getCards(idLesson) async {
    if (await DBProvider.bd.getCards(idLesson) != null) {
      return await DBProvider.bd.getCards(idLesson);
    } else {
      var data;
      SharedPreferences localStorage = await SharedPreferences.getInstance();
      var token = localStorage.getString('token');
      var response = await http.get(
          Uri.encodeFull(_url + "client/user/lessonCards/" + idLesson),
          headers: {
            "Accept": "application/json",
            "Authorization": "Bearer " + token
          });
      if (response.statusCode == 200) {
        data = json.decode(response.body)['data'];

        for (int i = 0; i < data.length; i++) {
          await DBProvider.bd.newCards(new Cards(
              id_lesson_card: data[i]["id_lesson_card"].toString(),
              id_lesson: data[i]["id_lesson"].toString(),
              cat_card_type: data[i]["cat_card_type"],
              id_key_item: data[i]["id_key_item"]));
        }
      }
      return data;
    }
  }

  getWordImage(wordId) async {
    if (await DBProvider.bd.getWordsImage(wordId) != null) {
      //print(await DBProvider.bd.getWordsImage(wordId));
      return await DBProvider.bd.getWordsImage(wordId);
    } else {
      var dataWordImage;
      SharedPreferences localStorage = await SharedPreferences.getInstance();
      var token = localStorage.getString('token');
      var response = await http
          .get(Uri.encodeFull(_url + "client/medias/" + wordId), headers: {
        "Accept": "application/json",
        "Authorization": "Bearer " + token
      });
      if (response.statusCode == 200) {
        dataWordImage = json.decode(response.body)['data'];
       // print(dataWordImage);
        for (int i = 0; i < 1; i++) {
          await DBProvider.bd.newWordsImage(new WordsImage(
              id_dictionary_word: dataWordImage[i]["id_dictionary_word"],
              path: dataWordImage[i]["path"]));
        }
        WordsImage wordsImage = dataWordImage.isNotEmpty
            ? WordsImage.fromMap(dataWordImage.first)
            : null;
        return wordsImage;
        //return dataWordImage;
      }
    }
  }

  getWordAudio(wordId) async {
    if (await DBProvider.bd.getWordsAudio(wordId) != null) {
      print('============AUDIO===============6');
      return await DBProvider.bd.getWordsAudio(wordId);
    } else {
      var dataWordAudio;
      SharedPreferences localStorage = await SharedPreferences.getInstance();
      var token = localStorage.getString('token');
      var response = await http.get(
          Uri.encodeFull(_url + "client/audios/" + wordId + "/"),
          headers: {
            "Accept": "application/json",
            "Authorization": "Bearer " + token
          });
      if (response.statusCode == 200) {
        print('============AUDIO===============');
        dataWordAudio = json.decode(response.body)['data'];
        await DBProvider.bd.newWordsAudio(new WordsAudio(
        id_dictionary_word: wordId,
        path: dataWordAudio[0]['path']));
          WordsImage wordsImage = dataWordAudio.isNotEmpty
            ? WordsImage.fromMap(dataWordAudio.first)
            : null;
        return wordsImage;
      }
     // return response;
    }
  }

  getDictionaryWords(wordId) async {
    if (await DBProvider.bd.getDictionaryWords(wordId) != null) {
      print('============== Dictionary Words =============6');
      return await DBProvider.bd.getDictionaryWords(wordId);
    } else {
      var dataDictionaryWords;
      SharedPreferences localStorage = await SharedPreferences.getInstance();
      var token = localStorage.getString('token');
      var response = await http.get(
          Uri.encodeFull(_url + "client/dictionaryWords/" + wordId + "/"),
          headers: {
            "Accept": "application/json",
            "Authorization": "Bearer " + token
          });
        dataDictionaryWords = json.decode(response.body)['data'];
        await DBProvider.bd.newDictionaryWords(new DictionaryWords(
        id_dictionary_word: wordId,
        grapheme_target: dataDictionaryWords[0]['grapheme_target'],
        grapheme_source: dataDictionaryWords[0]['grapheme_source']));
          DictionaryWords wordsImage = dataDictionaryWords.isNotEmpty
            ? DictionaryWords.fromMap(dataDictionaryWords.first)
            : null;
        return wordsImage;
      }
     // return response;
   }
}
