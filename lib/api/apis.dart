import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'package:tutorial_project/Models/WordsImage.dart';
import 'package:tutorial_project/Models/WordsAudio.dart';
import 'package:tutorial_project/Models/DictionaryWords.dart';

import 'package:tutorial_project/Models/Cards.dart';

import 'package:tutorial_project/db/database.dart';

import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqlite_api.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class CallApis {
   final String _url = 'https://back.tecnicos.lenguas.org.bo/api/v1/';
 // final String _url = 'https://back.lenguas.org.bo/api/v1/';
 //  final String _url = 'https://back.lenguas.oei.bo/api/v1/';

  postData(data, apiUrl) async {
    var fullUrl = _url + apiUrl + await _getToken();
    return await http.post(fullUrl,
        body: jsonEncode(data), headers: _setHeaders());
  }
  getData(apiUrl) async {
    var fullUrl = _url + apiUrl + await _getToken();
    return await http.get(fullUrl, headers: _setHeaders());
  }

  _setHeaders() => {
        'OEI-Token': 'app',
        'Content-type': 'application/json',
        'Accept': 'application/json',
      };
  _setHeadersU() => {
        'Content-type': 'application/json',
        'Accept': 'application/json',
      };
  _getToken() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var token = localStorage.getString('token');
    return '?token=$token';
  }

  getLanguageList() async {
    return getDadaApi("client/user/enrolledCourses?includeBeta=true", "", "");
  }

  getUnidadList(version) async {
    return getDadaApi("client/user/levels/", version, "?include=themes");
  }

  getLessonList(id_theme) async {
    return getDadaApi("client/user/lessons/", id_theme, "");
  }

  getScoreLesson(id_theme) async {
    return getDadaApi("client/user/lessonsDetail/", id_theme, "");
  }

  getCards(idLesson) async {
    return getDadaCards("client/user/lessonCards/", idLesson, "/");
  }

  getWordImage(wordId) async {
    return getDataWordImage("client/medias/", wordId, "");
  }

  getWordAudio(wordId) async {
    return getDataWordAudio("client/audios/", wordId, "");
  }

  getDictionaryWords(wordId) async {
    return getDataDictionaryWords("client/dictionaryWords/", wordId, "");
  }
  getDadaApi(api1, id, api2) async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var token = localStorage.getString('token');
    var response = await http.get(Uri.encodeFull(_url + api1 + id + api2),
        headers: {
          "Accept": "application/json",
          "Authorization": "Bearer " + token
        });
    // print(json.decode(response.body)['data']);
    return response;
    //return json.decode(response.body)['data'];
  }

  getSearchList() async {
   // var token = localStorage.getString('token');   client/search/Aymara/es_/papa
   /* var response = await http.get(Uri.encodeFull(_url + "client/search/"+language_or+"/es_/"+grapheme),
      headers: {"Accept": "application/json"}
       );*/
    var response = await http.get(
          Uri.encodeFull("https://back.tecnicos.lenguas.org.bo/api/v1/client/newsall"),
        headers: {"Accept": "application/json"}
    );
    return response;
  }

  getDadaApiJson(api1, id, api2) async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var token = localStorage.getString('token');
    var response = await http.get(Uri.encodeFull(_url + api1 + id + api2),
        headers: {
          "Accept": "application/json",
          "Authorization": "Bearer " + token
        });
    // print(json.decode(response.body)['data']);
    //print(response.length);
    //return response;
    if (response.statusCode == 200) {
      return json.decode(response.body)['data'];
    }
    return json.decode(response.body)['data'];
  }

  getDadaCards(api1, id, api2) async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var token = localStorage.getString('token');
    var response = await http.get(Uri.encodeFull(_url + api1 + id + api2),
        headers: {
          "Accept": "application/json",
          "Authorization": "Bearer " + token
        });
    var data;
    if (response.statusCode == 200) {
      data = json.decode(response.body)['data'];
      for (int i = 0; i < data.length; i++) {
          await DBProvider.bd.newCards(new Cards(
              id_lesson_card: data[i]["id_lesson_card"].toString(),
              id_lesson: data[i]["id_lesson"].toString(),
              cat_card_type: data[i]["cat_card_type"].toString(),
              id_key_item: data[i]["id_key_item"].toString(),
              option_dict_word_1: data[i]["option_dict_word_1"].toString(),
              option_dict_word_2: data[i]["option_dict_word_2"].toString(),
              option_dict_word_3: data[i]["option_dict_word_3"].toString(),
            ),  
          );
      }
    }
    return json.decode(response.body)['data'];
  }

  cacheImage(id_key_item) async {
    if (await DBProvider.bd.getWordsImage(id_key_item) == null) {
      print('============IMAGEN=============== APIsss');
      print(id_key_item);
      SharedPreferences localStorage = await SharedPreferences.getInstance();
      var token = localStorage.getString('token');
      var response = await http
          .get(Uri.encodeFull(_url + "client/medias/" + id_key_item), 
        headers: {
        "Accept": "application/json",
        "Authorization": "Bearer " + token
      });
      var data;
      if (response.statusCode == 200) {
        data = json.decode(response.body)['data'];
        print(data);
        //for (int i = 0; i < data.length; i++) {
        await DBProvider.bd.newWordsImage(
          new WordsImage(
            id_dictionary_word: id_key_item,
            path: data[0]["path"]));
         }
    
    }
  }

  cacheAudio(id_key_item) async {
    if (await DBProvider.bd.getWordsAudio(id_key_item) == null) {
      print('============AUDIO=============== APIsss');
      print(id_key_item);
      SharedPreferences localStorage = await SharedPreferences.getInstance();
      var token = localStorage.getString('token');
      var response = await http
          .get(Uri.encodeFull(_url + "client/audios/" + id_key_item), 
      headers: {
        "Accept": "application/json",
        "Authorization": "Bearer " + token
      });
      var data;
      data = json.decode(response.body)['data'];
             print(data);

      await DBProvider.bd.newWordsAudio(
          new WordsAudio(
            id_dictionary_word: id_key_item, 
            path: data['path']));
    }
  }

  cacheDictionaryWords(id_key_item) async {
    if (await DBProvider.bd.getDictionaryWords(id_key_item) == null) {
      print('============== Dictionary Words ============= APIsss');
            print(id_key_item);

      SharedPreferences localStorage = await SharedPreferences.getInstance();
      var token = localStorage.getString('token');
      var response = await http.get(
          Uri.encodeFull(_url + "client/dictionaryWords/" + id_key_item+ "/"),
          headers: {
            "Accept": "application/json",
            "Authorization": "Bearer " + token
          });
      var data;
      if (response.statusCode == 200) {
        data = json.decode(response.body)['data'];
                print(data);

        await DBProvider.bd.newDictionaryWords(
          new DictionaryWords(
            id_dictionary_word: id_key_item,
            grapheme_target: data[0]['grapheme_target'],
            grapheme_source: data[0]['grapheme_source']));
      }
    }
}















  getDataWordImage(api1, id, api2) async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var token = localStorage.getString('token');
    var response = await http.get(Uri.encodeFull(_url + api1 + id + api2),
        headers: {
          "Accept": "application/json",
          "Authorization": "Bearer " + token
        });
    var data;
    if (response.statusCode == 200) {
      data = json.decode(response.body)['data'];
      for (int i = 0; i < data.length; i++) {
        await DBProvider.bd.newWordsImage(new WordsImage(
            id_dictionary_word: data[i]["id_dictionary_word"],
            path: data[i]["path"]));
      }
    }
    WordsImage wordsImage =
        data.isNotEmpty ? WordsImage.fromMap(data.first) : null;
    return wordsImage;
  }

  getDataWordAudio(api1, id, api2) async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var token = localStorage.getString('token');
    var response = await http.get(Uri.encodeFull(_url + api1 + id + api2),
        headers: {
          "Accept": "application/json",
          "Authorization": "Bearer " + token
        });
    var data;
    if (response.statusCode == 200) {
   // print('============AUDIO=============== API');
    data = json.decode(response.body)['data'];
    // for (int i = 0; i < data.length; i++) {
    await DBProvider.bd.newWordsAudio(
        new WordsAudio(
        id_dictionary_word: id, 
        path: data['path']));
       }
    // }
    WordsAudio wordsAudio =
        data.isNotEmpty ? WordsAudio.fromMap(data) : null;
    return wordsAudio;
  }

  getDataDictionaryWords(api1, id, api2) async {
    print('============== Dictionary Words ============= API');
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var token = localStorage.getString('token');
    var response = await http.get(Uri.encodeFull(_url + api1 + id + api2),
        headers: {
          "Accept": "application/json",
          "Authorization": "Bearer " + token
        });
    var data;
    if (response.statusCode == 200) {
      data = json.decode(response.body)['data'];
      await DBProvider.bd.newDictionaryWords(new DictionaryWords(
          id_dictionary_word: id.toString(),
          grapheme_target: data[0]['grapheme_target'],
          grapheme_source: data[0]['grapheme_source']));
    }
    DictionaryWords dictionaryWords = data.isNotEmpty
            ? DictionaryWords.fromMap(data.first)
            : null;
    return dictionaryWords;
  }
}
