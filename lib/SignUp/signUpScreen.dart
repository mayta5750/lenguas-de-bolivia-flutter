import 'dart:convert';
import 'package:tutorial_project/Theme.dart' as Theme;
import 'package:flutter/material.dart';
import 'package:tutorial_project/Home/homeScreen.dart';
import 'package:tutorial_project/Login/loginScreen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tutorial_project/api/apis.dart';
import 'Dart:async';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController aliasController = TextEditingController();
  TextEditingController mailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  //TextEditingController phoneController = TextEditingController();
  DateTime selectedDate = DateTime.now();
  bool _isLoading = false;
  String gender='';
  ScaffoldState scaffoldState;
  _showMsg(msg) { //
    final snackBar = SnackBar(
      content: Text(msg),
      action: SnackBarAction(
        label: '',
        onPressed: () {
          // Some code to undo the change!
        },
      ),
    );
    Scaffold.of(context).showSnackBar(snackBar);
   }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF6ebdf1 ), 
      body: 
       Container(
        
        margin: EdgeInsets.only(top: 50),
        child:SingleChildScrollView(
        child: Stack(
          children: <Widget>[
            /////////////  background/////////////
            new Container(
              decoration: new BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  stops: [0.0, 0.4, 0.9],
                  colors: [
                   Color(0xFFeafaf1),
                    Color(0xFFffffff),
                    Color(0xFF0b98f7),  
                  ],
                ),
              ),
            ),
           
            Positioned(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Card(
                      elevation: 4.0,
                      color: Colors.white,
                      margin: EdgeInsets.only(left: 20, right: 20),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15)),
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: SingleChildScrollView(
                        child:
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            /////////////// name////////////
                            TextField(
                              style: TextStyle(color: Color(0xFF000000)),
                              controller: firstNameController,
                              cursorColor: Color(0xFF9b9b9b),
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                prefixIcon: Icon(
                                  Icons.account_circle,
                                  color: Colors.grey,
                                ),
                                hintText: "Nombre",
                                hintStyle: TextStyle(
                                    color: Color(0xFF9b9b9b),
                                    fontSize: 15,
                                    fontWeight: FontWeight.normal),
                              ),
                            ),
                            TextField(
                              style: TextStyle(color: Color(0xFF000000)),
                              controller: lastNameController,
                              cursorColor: Color(0xFF9b9b9b),
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                prefixIcon: Icon(
                                  Icons.account_circle,
                                  color: Colors.grey,
                                ),
                                hintText: "Apellido",
                                hintStyle: TextStyle(
                                    color: Color(0xFF9b9b9b),
                                    fontSize: 15,
                                    fontWeight: FontWeight.normal),
                              ),
                            ),
                            TextField(
                              style: TextStyle(color: Color(0xFF000000)),
                              controller: aliasController,
                              cursorColor: Color(0xFF9b9b9b),
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                prefixIcon: Icon(
                                  Icons.label,
                                  color: Colors.grey,
                                ),
                                hintText: "Alias",
                                hintStyle: TextStyle(
                                    color: Color(0xFF9b9b9b),
                                    fontSize: 15,
                                    fontWeight: FontWeight.normal),
                              ),
                            ),
                            /////////////// Email ////////////

                            TextField(
                              style: TextStyle(color: Color(0xFF000000)),
                              controller: mailController,
                              cursorColor: Color(0xFF9b9b9b),
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                prefixIcon: Icon(
                                  Icons.mail,
                                  color: Colors.grey,
                                ),
                                hintText: "Correo Electronico ",
                                hintStyle: TextStyle(
                                    color: Color(0xFF9b9b9b),
                                    fontSize: 15,
                                    fontWeight: FontWeight.normal),
                              ),
                            ),

                           /*TextField(
                              style: TextStyle(color: Color(0xFF000000)),
                              controller: phoneController,
                              cursorColor: Color(0xFF9b9b9b),
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                prefixIcon: Icon(
                                  Icons.mobile_screen_share,
                                  color: Colors.grey,
                                ),
                                hintText: "Phone",
                                hintStyle: TextStyle(
                                    color: Color(0xFF9b9b9b),
                                    fontSize: 15,
                                    fontWeight: FontWeight.normal),
                              ),
                            ),*/

                            /////////////// SignUp Button ////////////
Container(
  child: Row(
    mainAxisSize: MainAxisSize.min,
    children: <Widget>[  
      Flexible(
          fit: FlexFit.loose,
          child:RadioListTile<String>(
              title: const Text('Hombre',
                style: TextStyle(
                  fontSize: 15.0,),
                ),
              value: 'hombre',
              groupValue: gender,
              onChanged: (value) {
                setState(() {
                  gender = value;
                });
              },
          ),
         ),
          Flexible(
          fit: FlexFit.loose,
        child:RadioListTile<String>(
            title: const Text('Mujer',
            style: TextStyle(
              fontSize: 15.0,),
            ),
            value: 'mujer',
        groupValue: gender,
       onChanged: (value) {
         setState(() {
           gender = value;
         });
       }
        ),
     )
   ]),
   ),
  Padding(
    padding: const EdgeInsets.only(top: 20),
    child: InkWell(
    onTap: () => _selectDate(context),
    child: ListTile(
        leading: const Icon(Icons.today),
        title: const Text('Fecha de Nacimiento'),
        subtitle: Text("${selectedDate.day.toString().padLeft(2,'0')}-${selectedDate.month.toString().padLeft(2,'0')}-${selectedDate.year.toString()}"),
    ),
  ),
),

                            /////////////// password ////////////

                            TextField(
                              style: TextStyle(color: Color(0xFF000000)),
                              cursorColor: Color(0xFF9b9b9b),
                              controller: passwordController,
                              keyboardType: TextInputType.text,
                              obscureText: true,
                              decoration: InputDecoration(
                                prefixIcon: Icon(
                                  Icons.vpn_key,
                                  color: Colors.grey,
                                ),
                                hintText: "Contraseña",
                                hintStyle: TextStyle(
                                    color: Color(0xFF9b9b9b),
                                    fontSize: 15,
                                    fontWeight: FontWeight.normal),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: FlatButton(
                                child: Padding(
                                  padding: EdgeInsets.only(
                                      top: 8, bottom: 8, left: 10, right: 10),
                                  child: Text(
                                   _isLoading ? 'CREANDO...' : 'CREAR CUENTA',
                                    textDirection: TextDirection.ltr,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 15.0,
                                      decoration: TextDecoration.none,
                                      fontWeight: FontWeight.normal,
                                    ),
                                  ),
                                ),
                                color: Theme.Colors.buttonIniciar,
                                disabledColor: Theme.Colors.buttonIniciando,
                                shape: new RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(20.0)),
                                onPressed: _isLoading ? null :  _handleLogin
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    ),
                    /////////////// already have an account ////////////
                    Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: InkWell(
                        onTap: () {
                          Navigator.pop(context);
                          /*Navigator.push(
                              context,
                              new MaterialPageRoute(
                                  builder: (context) => LogIn()));*/
                        },
                        child: Text(
                          'Ya tengo una cuenta',
                          textDirection: TextDirection.ltr,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 15.0,
                            decoration: TextDecoration.none,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
      ),
    );
  }
    Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(1950, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }
  void _handleLogin() async {
    setState(() {
       _isLoading = true; 
    });
       /* String name = mNameText.getText().toString();
        String surname = mSurnameText.getText().toString();
        String email = mEmailText.getText().toString();
        String alias = mAliasText.getText().toString();
        String birthDate = getBirthDate();
        String password = mPasswordText.getText().toString();
        String repeatPassword = mRepeatPasswordText.getText().toString();*/

        /*
           this.email = email;
            this.password = password;
            this.firstName = firstName;
            this.firstSurname = firstSurname;
            this.alias = alias;
            this.gender = gender;
            this.birthDate = birthDate;
         */
        
    var data = {
      'user' :{
        'email' : mailController.text,
        'password' : passwordController.text,
        'first_name' : firstNameController.text,
        'first_surname' : lastNameController.text,
        'alias' : aliasController.text,
        'gender' : gender,
        'birth_date' : "${selectedDate.year.toString()}-${selectedDate.month.toString().padLeft(2,'0')}-${selectedDate.day.toString().padLeft(2,'0')}",
      }
    };
    /*var data = {
      'user' :{
        'email' : 'adc@oei.bo',
        'password' : '123456',
        'first_name' : 'joes',
        'first_surname' : 'jajajasd',
        'alias' : 'aengop',
        'gender' : 'hombre',
        'birth_date' : '1983-05-26',
      }
    };*/

    var res = await CallApis().postData(data, 'users');
    var body = json.decode(res.body);
    //print(body);
    if(body['error']==null){
      print('asdasdasd');
      //SharedPreferences localStorage = await SharedPreferences.getInstance();
      //localStorage.setString('token', body['data']['token']);
      var res = await CallApis().getData('users/self?include=person');
      var inicia = {"data":{"id_lesson":0,"id_version":1,"id_user":2151}};
      await CallApis().postData(inicia, 'client/user/enroll');
      //localStorage.setString('user', json.encode(body['user']));
    }else{
      print(body['error']['detail']);
      print(body['error']['detail']['email']);
      print(body['error']['detail']['password']);
      print(body['error']['detail']['alias']);
      print(body['error']['detail']['first_name']);
      print(body['error']['detail']['first_surname']);
      String a = body['error']['detail']['email']==null?'':'Correo no válido\n';
      String b = body['error']['detail']['password']==null?'':'Contraseña mínimo de 6 letras\n';
      String c = body['error']['detail']['alias']==null?'':'Intenta con otro Alias\n';
      String d = body['error']['detail']['first_name']==null?'':'Requiere Nombre\n';
      String e = body['error']['detail']['first_surname']==null?'':'Requiere Apellido\n';
     // _showDialog(e+"\n"+d+"\n"+c+"\n"+a+"\n"+b);
      _showDialog(e+d+c+a+b);

      //_showMsg('bn');
    }
   /* if(body['success']){
      SharedPreferences localStorage = await SharedPreferences.getInstance();
      localStorage.setString('token', body['token']);
      localStorage.setString('user', json.encode(body['user']));
      
       Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (context) => Home()));
    }*/




    setState(() {
       _isLoading = false; 
    });
    
    
    
  }
 void _showDialog(String a) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Mensaje"),
          content: new Text(a),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Cerrar"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
}
   
}
