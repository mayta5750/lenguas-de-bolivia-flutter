import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:tutorial_project/Theme.dart' as Theme;
import 'dart:convert';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fake_okhttp/fake_okhttp.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tutorial_project/api/data.dart';
import 'package:tutorial_project/UnidadList/UnidadListScreen.dart';
import 'package:tutorial_project/Search/searchListScreen.dart';

import 'package:shared_preferences/shared_preferences.dart';

class SelectionList extends StatelessWidget {
  SelectionList({Key key, this.idioma, this.id_version}) : super(key: key);
  final String idioma;
  final String id_version;

  @override
  Widget build(BuildContext context) {
        return Scaffold(
      appBar: AppBar(
        title: Text(idioma),
        backgroundColor: Theme.Colors.barBackground,
        leading: IconButton(icon:Icon(Icons.arrow_back),
        //color: Colors.black,
        onPressed:() => Navigator.pop(context)),
      ),
      body: MySelectionListPage(id_version:id_version,idioma: idioma),
    );
   /* return MaterialApp(
      title: 'Lenguas de Bolivia',
      theme: ThemeData(
          // primarySwatch: Colors.appBarTitle,
          ),
      home: MySelectionListPage(title: idioma),
    );*/
  }
}

class MySelectionListPage extends StatefulWidget {
  MySelectionListPage({Key key, this.idioma, this.id_version}) : super(key: key);

  final String idioma;
  final String id_version;

  @override
  _MySelectionListPageState createState() => _MySelectionListPageState();
}

class _MySelectionListPageState extends State<MySelectionListPage> {
  List data;

  // Function to get the JSON data
  Future<String> getJSONData() async {
    setState(() {
      print(widget.id_version);
       data =[{
        'language' : "¡APRENDIZAJE!", 
        'icon_url' : "assets/ops/academia.png",
        'detail':''
    },{
      //  'description':"¡BUSQUEDA!",
      
        'language' : "¡BUSQUEDA!", 
        'icon_url' : "assets/ops/busqueda.png",
         'detail':''
    }
    ];
  
    });
    return "Successfull";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
     /* appBar: AppBar(
        title: Text(widget.title),
        backgroundColor: Theme.Colors.barBackground,
      ),*/
      backgroundColor: Theme.Colors.bodyBackground,
      body: CustomScrollView(
        slivers: <Widget>[
          // Agrega un app bar al CustomScrollView
          SliverAppBar(
            backgroundColor: Theme.Colors.background,
            // Provee un título estándard
            title: Text('OEI'),
            // Permite al usuario revelar el app bar si comienza a desplazarse
            // hacia arriba en la lista de elementos
            floating: true,
            // Mostrar un widget placeholder para visualizar el tamaño de reducción
            flexibleSpace: FlexibleSpaceBar(
              centerTitle: true,
              title: Text("",
                  //title: Text("Elige un curso y \n continúa aprendiendo",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16.0,
                  )),
              background: Image(
                image: AssetImage('assets/ops/baner_selection.jpeg'),
                width: 40,
                height: 20,
              ),
            ),
            // Aumentar la altura inicial de la SliverAppBar más de lo normal
            expandedHeight: 190,
          ),
          // A continuación, crea un SliverList
          SliverList(
            // Para ello, se utiliza un delegado para crear elementos a medida que
            // se desplazan por la pantalla.

            delegate: SliverChildBuilderDelegate(
              // La función builder devuelve un ListTile con un título que
              // muestra el índice del elemento actual
              (context, index) => _buildImageColumn(data[index]),
              // Construye 1000 ListTiles
              childCount: data == null ? 0 : data.length,
            ),
          ),
        ],
      ),
      // _buildListView(),
    );
  }
  Widget _buildListView() {
    return ListView.builder(
        padding: const EdgeInsets.all(16.0),
        itemCount: data == null ? 0 : data.length,
        itemBuilder: (context, index) {
          return _buildImageColumn(data[index]);
          // return _buildRow(data[index]);
        });
  }
  Widget _buildImageColumn(dynamic item) => Container(
        padding: const EdgeInsets.all(14.0),
        decoration: BoxDecoration(color: Colors.white54),
        margin: const EdgeInsets.all(0),
        child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10.0),
          ),
        ),
          color: Theme.Colors.searchCard,
            child: Row(
          children: <Widget>[
            Card(
              shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10.0),
          ),
        ),
              color: Theme.Colors.searchIcon,
              child:Padding(
              padding: const EdgeInsets.all(16.0),
              child:Image(
                color: Theme.Colors.background,
                image: AssetImage(item['icon_url']),
                height: 70.0,
                width: 70.00,
              )
            ),
            ),
           
            Expanded(
              child: GestureDetector(
                  onTap: () {
                  switch (item['language']) {
                    case '¡APRENDIZAJE!':
                      return _onUnidad(item);
                    case '¡BUSQUEDA!':
                      return _onSearch(item);
                }
              },
              child: _buildRow(item)),
            ),
          ],
        )),
      );
  Widget _buildRow(dynamic item) {
    return ListTile(
      title: Text(item['language'] == null ? '' : item['language'],
          style: TextStyle(
            //color: Colors.white,
            fontWeight: FontWeight.bold,
            fontFamily: "Poppins-Bold",
            fontSize: 22,
            letterSpacing: 2.0,
            wordSpacing: 20.0,
          )
      )
    );
  }
  @override
  void initState() {
    super.initState();
    // Call the getJSONData() method when the app initializes
    this.getJSONData();
  }
  void _onUnidad(dynamic item) async {
      SharedPreferences localStorage = await SharedPreferences.getInstance();
      //localStorage.setString('languageList', json.encode(item));
      localStorage.setString('id_version', widget.id_version);
          Navigator.push(
        context, MaterialPageRoute(builder: (context) => UnidadList(id_version:widget.id_version,idioma:widget.idioma)));
  }
  void _onSearch(dynamic item) async {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => SearchList(id_version:widget.id_version,idioma:widget.idioma)));
  }
}
