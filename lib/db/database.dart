import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqlite_api.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:tutorial_project/Models/WordsImage.dart';
import 'package:tutorial_project/Models/WordsAudio.dart';
import 'package:tutorial_project/Models/DictionaryWords.dart';


import 'package:tutorial_project/Models/Cards.dart';


class DBProvider {
  DBProvider._();
  static final DBProvider bd = DBProvider._();
  Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await initDB();
    return _database;
  }
  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "Suspensions.db");
    return await openDatabase(path,
        version: 100, onOpen: (db) {}, onCreate: _createTables);
  }

  void _createTables(Database db, int version) async {
    await db.execute("CREATE TABLE wordsMedia ("
        "id_dictionary_word VARCHAR(100) NOT NULL,"
        "path VARCHAR(2000),"
        "CONSTRAINT id_unique UNIQUE (id_dictionary_word))"
    
        );

    await db.execute("CREATE TABLE Cards ("
        "id_lesson_card VARCHAR(100) NOT NULL,"
        "id_lesson VARCHAR(100) NOT NULL,"
        "cat_card_type VARCHAR(100) NOT NULL,"
        "id_key_item VARCHAR(100) NOT NULL,"
        "option_dict_word_1 VARCHAR(100),"
        "option_dict_word_2 VARCHAR(100),"
        "option_dict_word_3 VARCHAR(100),"
        "CONSTRAINT id_unique UNIQUE (id_lesson_card))");

    await db.execute("CREATE TABLE wordsAudio ("
        "id_dictionary_word VARCHAR(100) NOT NULL,"
        "path VARCHAR(2000),"
        "CONSTRAINT id_unique UNIQUE (id_dictionary_word))");

    await db.execute("CREATE TABLE dictionaryWords ("
        "id_dictionary_word VARCHAR(100) NOT NULL,"
        "grapheme_target VARCHAR(2000),"
        "grapheme_source VARCHAR(2000),"
        "CONSTRAINT id_unique UNIQUE (id_dictionary_word))");
  }
  newWordsImage(WordsImage newWordsImage) async {
    final db = await database;
    // var res = await db.insert("measurers", newMeasurer.toMap());

    var res = await db.rawInsert(
        "INSERT INTO wordsMedia (id_dictionary_word, path)"
        " VALUES (?,?)",
        [
          newWordsImage.id_dictionary_word,
          newWordsImage.path,
        ]);

    return res;
  }
  getWordsImage(String id_dictionary_word) async {
    final db = await database;
    var res = await db.rawQuery("SELECT * FROM wordsMedia WHERE id_dictionary_word='"+id_dictionary_word+"'");
   /* List<WordsImage> list =
        res.isNotEmpty ? res.toList().map((c) => WordsImage.fromMap(c)) : null;*/
     
        //res.isNotEmpty ? res.map((c) => WordsImage.fromMap(c)) : null;
       WordsImage wordsImage = res.isNotEmpty ? WordsImage.fromMap(res.first):null;
    return wordsImage;
  }

/* ====================================cards======================================0 */

  newCards(Cards newCards) async {
    final db = await database;
    // var res = await db.insert("measurers", newMeasurer.toMap());
    var res = await db.rawInsert(
        "INSERT INTO Cards (id_lesson_card, id_lesson, cat_card_type, id_key_item, option_dict_word_1, option_dict_word_2, option_dict_word_3)"
        " VALUES (?,?,?,?,?,?,?)",
        [
          newCards.id_lesson_card,
          newCards.id_lesson,
          newCards.cat_card_type,
          newCards.id_key_item,
          newCards.option_dict_word_1,
          newCards.option_dict_word_2,
          newCards.option_dict_word_3,
        ]);

    return res;
  }
  getCards(String id_lesson) async {
    final db = await database;
    var res = await db.rawQuery("SELECT * FROM Cards WHERE id_lesson='"+id_lesson+"'");
   /* List<WordsImage> list =
        res.isNotEmpty ? res.toList().map((c) => WordsImage.fromMap(c)) : null;*/
    return res.isNotEmpty? res:null;
  }

  /*===============================AUDIO==================================*/
  newWordsAudio(WordsAudio newWordsAudio) async {
      final db = await database;
      var res = await db.rawInsert(
          "INSERT INTO wordsAudio (id_dictionary_word, path)"
          " VALUES (?,?)",
          [
            newWordsAudio.id_dictionary_word,
            newWordsAudio.path,
          ]);

      return res;
    }
   getWordsAudio(String id_dictionary_word) async {
      final db = await database;
      var res = await db.rawQuery("SELECT * FROM wordsAudio WHERE id_dictionary_word='"+id_dictionary_word+"'");
      WordsAudio wordsAudio = res.isNotEmpty ? WordsAudio.fromMap(res.first):null;
    //return res.isNotEmpty? res:null;
    return wordsAudio;
  }
    /*===============================dictionaryWords==================================*/
  newDictionaryWords(DictionaryWords newDictionaryWords) async {
      final db = await database;
      var res = await db.rawInsert(
          "INSERT INTO dictionaryWords (id_dictionary_word, grapheme_target, grapheme_source)"
          " VALUES (?,?,?)",
          [
            newDictionaryWords.id_dictionary_word,
            newDictionaryWords.grapheme_target,
            newDictionaryWords.grapheme_source,
          ]);

      return res;
    }
   getDictionaryWords(String id_dictionary_word) async {
      final db = await database;
      var res = await db.rawQuery("SELECT * FROM dictionaryWords WHERE id_dictionary_word='"+id_dictionary_word+"'");
      DictionaryWords dictionaryWords = res.isNotEmpty ? DictionaryWords.fromMap(res.first):null;
    //return res.isNotEmpty? res:null;
    return dictionaryWords;
  }
}