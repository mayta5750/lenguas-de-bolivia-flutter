import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:math';
import 'package:loading/loading.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'dart:io';

import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tutorial_project/Cards/CardsScreen.dart';
import 'package:tutorial_project/api/apis.dart';
import 'package:tutorial_project/api/data.dart';

import 'package:tutorial_project/UnidadList/UnidadListScreen.dart';

import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:tutorial_project/Theme.dart' as Theme;
import 'package:percent_indicator/percent_indicator.dart';
import 'package:tutorial_project/Cards/card/card.dart';
import 'package:tutorial_project/Models/WordsImage.dart';
import 'package:tutorial_project/Models/WordsAudio.dart';
import 'package:tutorial_project/Models/DictionaryWords.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:audioplayer/audioplayer.dart';
import 'package:tutorial_project/Cards/card/fin_fragment.dart';

class Cards extends StatelessWidget {
  Cards({Key key, this.idLesson, this.description}) : super(key: key);
  final String idLesson;
  final String description;

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /*appBar: AppBar(
        title: Text('Unidades'),
        backgroundColor: Theme.Colors.barBackground,
        leading: IconButton(icon:Icon(Icons.arrow_back),
        //color: Colors.black,
        onPressed:() => Navigator.pop(context)),
      ),*/
      body: MyCardsPage(
        title: 'Unidades', 
        idLesson: this.idLesson, 
        description:this.description
      ),
    );
  }
}

class MyCardsPage extends StatefulWidget {
  MyCardsPage({Key key, this.title, this.idLesson, this.description}) : super(key: key);
  final String title;
  final String idLesson;
  final String description;
  @override
  _MyCardsPageState createState() => _MyCardsPageState();
}

enum PlayerState { stopped, playing, paused }

class _MyCardsPageState extends State<MyCardsPage>
    with SingleTickerProviderStateMixin {
  List data = [];
  List scoreLesson = [];
  List<dynamic> cards;
  var tinpal;
  bool _isLoading = false;
  bool sw = true;
  int i = 0;
  int index = -1;
  AnimationController _controller;
  
  bool _isPlaying = false;

  WordsImage wordImage;
  WordsAudio wordAudio;

  DictionaryWords wordDictionaryWord;
  DictionaryWords wordDictionaryWordOption_1;
  DictionaryWords wordDictionaryWordOption_2;

  String graphemeTarget;
  String graphemeSource;

  String graphemeTargetOption_1;
  String graphemeSourceOption_1;

  String graphemeTargetOption_2;
  String graphemeSourceOption_2;

  String pathImagen;
  String pathAudio;
  AudioPlayer audioPlayer = new AudioPlayer();
  AudioPlayer audioPlugin = new AudioPlayer();

  int a, b, c;

  String indice;

  PlayerState playerState = PlayerState.stopped;
  Animation<double> animation;
  AnimationController controller;
  Future<String> getJSONData() async {
    cards = await DataApi().getCards(widget.idLesson);
    indice = aleatorio();
    a = int.parse(indice.substring(0, 1));
    b = int.parse(indice.substring(1, 2));
    c = int.parse(indice.substring(2, 3));
    var dat = [{
      'id_lesson_card': '5', 
     'id_lesson': '1',
      'cat_card_type': widget.description,
       'id_key_item': '3-288-702-619',
        'option_dict_word_1': widget.description, 
        'option_dict_word_2': widget.description
      }];
      cards=cards+dat;
      print(cards);
    setState(() {});
    return "Successfull";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.Colors.bodyBackground,
      body:_buildContent(),
    );
  }

  Widget _buildContent() {
    // print(cards.length);

  return Padding(
        padding: EdgeInsets.only(top: 0, bottom: 0, left: 0, right: 0),
        child: cards == null
                ? _loading()
                : _buildLessonItem(),);
  }

  getCardTinpal(dynamic item) async {
    // WordsImage wordImage = await DataApi().getWordImage(id_key_item);
    /* await CallApis().cacheImage(id_key_item);
    await CallApis().cacheAudio(id_key_item);
    await CallApis().cacheDictionaryWords(id_key_item);*/
    wordImage = await DataApi().getWordImage(item['id_key_item']);
    wordAudio = await DataApi().getWordAudio(item['id_key_item']);
    wordDictionaryWord = await DataApi().getDictionaryWords(item['id_key_item']);
    setState(() {
      graphemeTarget = wordDictionaryWord.getGraphemeTarget();
      graphemeSource = wordDictionaryWord.getGraphemeSource();
      pathImagen = wordImage.getPath();
      pathAudio = wordAudio.getPath();
    });
  }

  getCardPrseiama(dynamic item) async {//2
    wordImage = await DataApi().getWordImage(item['id_key_item']);
    wordAudio = await DataApi().getWordAudio(item['id_key_item']);
    wordDictionaryWord = await DataApi().getDictionaryWords(item['id_key_item']);
    wordDictionaryWordOption_1 = await DataApi().getDictionaryWords(item['option_dict_word_1']);
    wordDictionaryWordOption_2 = await DataApi().getDictionaryWords(item['option_dict_word_2']);
    setState(() {
      graphemeSource = wordDictionaryWord.getGraphemeSource();
      graphemeTarget = wordDictionaryWord.getGraphemeTarget();
      pathAudio = wordAudio.getPath();
      pathImagen = wordImage.getPath();
      graphemeSourceOption_1 = wordDictionaryWordOption_1.getGraphemeSource();  
      graphemeSourceOption_2 = wordDictionaryWordOption_2.getGraphemeSource();    
    });
  }

  getCardPrasauej(dynamic item) async {//3
    wordAudio = await DataApi().getWordAudio(item['id_key_item']);
    wordDictionaryWord = await DataApi().getDictionaryWords(item['id_key_item']);
    wordDictionaryWordOption_1 = await DataApi().getDictionaryWords(item['option_dict_word_1']);
    setState(() {
      graphemeSource = wordDictionaryWord.getGraphemeSource();
      pathAudio = wordAudio.getPath();
      graphemeSourceOption_1 = wordDictionaryWordOption_1.getGraphemeSource();
      
    });
  }

  Widget _buildLessonItem() {
    /* ===================tarjeta================ */
      return(new CollectionCards(cards:cards,index:0));
  }
  
  Widget _loaing(){
    return Container(
      alignment: Alignment.center,
        child:Center(
        child: Image(
          alignment: Alignment.center,
          image: AssetImage('assets/ops/loading.png'),
          width: 50,
          height: 50,
          fit: BoxFit.cover)
      ),
    );
  }


  
  Widget _progresBar() {
    return Padding(
      padding: EdgeInsets.only(left: 15, right: 15, top: 15),
      child: new LinearPercentIndicator(
        width: MediaQuery.of(context).size.width - 30,
        animation: true,
        lineHeight: 15.0,
        animationDuration: 2000,
        percent: 1,
        // center: Text("90.0%"),
        linearStrokeCap: LinearStrokeCap.roundAll,
        progressColor: Colors.greenAccent,
      ),
    );
  }

/* ============================================================================ */

  Widget _tinpal() {
    return Container(
      // height: 200,
      height: MediaQuery.of(context).size.height - 50,
      width: MediaQuery.of(context).size.width,
      child: Card(
        elevation: 3,
        color: i%2==0?Theme.Colors.bodyCard_1:Theme.Colors.bodyCard_2,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(8.0),
          ),
        ),
        margin: const EdgeInsets.all(20),
        child: Stack(
          children: <Widget>[
            Positioned.fill(
              top: 30,
              child: Align(
                alignment: Alignment.topCenter,
                child: _graphemeSource(),
              ),
            ),
            Positioned.fill(
              bottom: 50,
              child: Align(
                alignment: Alignment.center,
                child: _imagen(),
              ),
            ),
            Positioned.fill(
              bottom: 100,
              child: Align(
                alignment: Alignment.bottomCenter,
                child: _graphemeTarget(),
              ),
            ),
            Positioned.fill(
              bottom: 25,
              child: Align(
                alignment: Alignment.bottomCenter,
                child: _eventButton('Siguiente'),
              ),
            ),
            Positioned(
              bottom: 150,
              right: 15,
              child: FloatingActionButton(
                onPressed: play,
                tooltip: 'Play',
                child: const Icon(Icons.play_arrow),
              ),
            ),
          ],
        ),
      ),
    );
  }


  Widget _prseiama() {
    return Container(
      // height: 200,
      height: MediaQuery.of(context).size.height - 50,
      width: MediaQuery.of(context).size.width,
      child: Card(
        elevation: 3,
        color: i%2==0?Theme.Colors.bodyCard_1:Theme.Colors.bodyCard_2,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(8.0),
          ),
        ),
        margin: const EdgeInsets.all(20),
        child: Stack(
          children: <Widget>[
            Positioned.fill(
              top: 30,
              child: Align(
                alignment: Alignment.topCenter,
                child: _textOption("Escoge el significado de:"),
              ),
            ),
            Positioned.fill(
              top: 60,
              child: Align(
                alignment: Alignment.topCenter,
                child: _graphemeTarget(),
              ),
            ),
            Positioned.fill(
              top: 120,
              child: Align(
                alignment: Alignment.topCenter,
                child: _imagen(),
              ),
            ),
            Positioned.fill(
              top: 370,
              child: Align(
                alignment: Alignment.bottomCenter,
                child: _collectionButtons(),
              ),
            ),
          ],
        ),
      ),
    );
  }

/* ========================================_prseiama=========================================== */
  Widget _collectionButtons() {
    List aux = [graphemeSource, graphemeSourceOption_1, graphemeSourceOption_2];
    return Column(
      children: <Widget>[
        _buttonPrseiama(aux[a] == null ? '' : aux[a]),
        _buttonPrseiama(aux[b] == null ? '' : aux[b]),
        _buttonPrseiama(aux[c] == null ? '' : aux[c]),
      ],
    );
  }

  Widget _buttonPrseiama(String text) {
    return SizedBox(
      width: MediaQuery.of(context).size.width - 80, // specific value
      child: new RaisedButton(
        child: Text(text,
            style: TextStyle(
                color: Colors.white,
                fontFamily: "Poppins-Bold",
                fontSize: 15,
                letterSpacing: 1.0)),
        elevation: 5.0,
        color: Theme.Colors.collectionButtons,
        onPressed: () {
         // load();
          if (text == graphemeSource) {
            _showMsg('Tu respuesta es correcta', '');
            //
          } else {
            _showMsg('La respuesta correcta es :', graphemeSource);
          }
         // load();
          asignarAleatorio();
          i++;
              //sleep(const Duration(milliseconds: 1000));
           /* setState(() {
                _loading();
                
              });*/
        
        },
      ),
    );
  }



  _showMsg(msg, msg2) {
    //
    final snackBar = SnackBar(
        //duration: const Duration(milliseconds: 100),

        backgroundColor:  msg2 == ''
            ? Theme.Colors.showMsgCorrecto
            : Theme.Colors.showMsgInCorrecto,
      
        content:

          Container(
            
            height: 150.0,
            width: MediaQuery.of(context).size.width+20,

            child: Column(

              children: <Widget>[
                msg2 == ''
                    ? Image(
                        image: AssetImage('assets/icons/bien.png'),
                        width: 50,
                        height: 50,
                        fit: BoxFit.cover)
                    : Image(
                        image: AssetImage('assets/icons/mal.png'),
                        fit: BoxFit.cover),
                Text(
                  msg,
                  style: TextStyle(
                      fontSize: 22.0,
                      color: Colors.white,
                      letterSpacing: 2.0,
                      fontWeight: FontWeight.bold),
                ),
                msg2 == ''
                    ? Text(msg2, style: TextStyle(fontSize: 0))
                    : Text(
                        msg2,
                        style: TextStyle(
                            fontSize: 24.0,
                            color: Colors.white,
                            letterSpacing: 1.0,
                            fontWeight: FontWeight.bold),
                      ),
                      //_eventButtonContinuar(),
              ],
            )
          
        )
         );
    Scaffold.of(context).showSnackBar(snackBar);

   
  }
/* ========================================_prseiama=========================================== */
/* ========================================_prasauej=========================================== */

  Widget _prasauej() {
    return Container(
      // height: 200,
      height: MediaQuery.of(context).size.height - 50,
      width: MediaQuery.of(context).size.width,
      child: Card(
        elevation: 3,
        color: i%2==0?Theme.Colors.bodyCard_1:Theme.Colors.bodyCard_2,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(8.0),
          ),
        ),
        margin: const EdgeInsets.all(20),
        child: Stack(
          children: <Widget>[
            Positioned.fill(
              top: 30,
              child: Align(
                alignment: Alignment.topCenter,
                child: _textOption("¿Cuál opción corresponde al audio?"),
              ),
            ),
            Positioned.fill(
              top: 100,
              
              child: Align(
                alignment: Alignment.topCenter,
                child: FloatingActionButton(
                onPressed: play,
                tooltip: 'Play',
                child: const Icon(Icons.play_arrow),
              ),
              ),
            ),
      
         Positioned.fill(
              bottom: 25,
              child: Align(
                alignment: Alignment.bottomCenter,
                child: _duoPalabra(),
              ),
            ),
          ],
        ),
      ),
    );
  }

Widget _duoPalabra(){
  return Center(
    child:Row(
        children: <Widget>[
        _palabra(graphemeSource),
        _palabra(graphemeSourceOption_1),
      ],
      )
 
  );
}
Widget _palabra(String palabra){
  return Container(
     margin: const EdgeInsets.all(15.0),
 // padding: const EdgeInsets.all(3.0),
  decoration: BoxDecoration(
   /* border: Border(
      left: BorderSide( //                   <--- left side
        color: Colors.blue[100],
        width: 3.0,
      ),
          top: BorderSide( //                   <--- left side
        color: Colors.blue[300],
        width: 3.0,
      ),
          right: BorderSide( //                   <--- left side
        color: Colors.blue[500],
        width: 3.0,
      ),
          bottom: BorderSide( //                   <--- left side
        color: Colors.blue[800],
        width: 3.0,
      ),
     
    ),*/
    border: Border.all(
      color:Colors.black,
    ),
    borderRadius: BorderRadius.all(
        Radius.circular(5.0) //                 <--- border radius here
    ),
  ),
    padding: EdgeInsets.only(top: 40,bottom: 40),

    width: 130,
    height: 120,
    child: Center(
    child:Text(
      palabra, style: TextStyle(
                      fontSize: 18.0,
                      color: Colors.black,
                      letterSpacing: 2.0,
                      fontWeight: FontWeight.bold),
    ),)
  );
}









  Widget prueba() {
    return  Stack(
        alignment: Alignment.center,
        children: <Widget>[
          _buildCircularContainer(100),
          _buildCircularContainer(150),
          _buildCircularContainer(200),
          Align(child: CircleAvatar(
            child: FloatingActionButton(
            
                onPressed: playPrasauej,
                tooltip: 'Play',
                child: const Icon(Icons.play_arrow),
          ), radius: 72)),
         /* Align(
            alignment: Alignment(0, 0.5),
            child: RaisedButton(
              child: Text(_isPlaying? "STOP" : "START"),
              onPressed: () {
                if (_isPlaying) _controller.reset();
                else _controller.repeat();
                setState(() => _isPlaying = !_isPlaying);
              },
            ),
          ),*/
        ],
      
    );
  }
    Widget _buildCircularContainer(double radius) {
    return AnimatedBuilder(
      animation: CurvedAnimation(parent: _controller, curve: Curves.fastLinearToSlowEaseIn),
      builder: (context, child) {
        return Container(
          width: _controller.value * radius,
          height: _controller.value * radius,
          decoration: BoxDecoration(color: Colors.black54.withOpacity(1 - _controller.value), shape: BoxShape.circle),
        );
      },
    );
  }
/* ========================================_prasauej=========================================== */

  Widget _graphemeSource() {
    return Padding(
        padding: EdgeInsets.only(top: 8, bottom: 8, left: 10, right: 10),
        child: Text(
          graphemeSource == null ? '' : graphemeSource,
          style: TextStyle(
              fontSize: 28.0,
              color: Theme.Colors.textSource,
              fontWeight: FontWeight.bold),
        ));
  }

  Widget _imagen() {
    return new SvgPicture.network(
      pathImagen == null
          ? 'http://web.oei.bo/dclo_public/img_out_L/L_ay_240518/5/1670.svg'
          : pathImagen,
      semanticsLabel: 'Feed button',
      height: 230.0,
      width: 230.00,
      fit: BoxFit.contain,
      placeholderBuilder: (context,) => Icon(Icons.error),
    );
  }
  Widget _graphemeTarget() {
    return Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8, left: 10, right: 10),
      child: Text(
        graphemeTarget == null ? '' : graphemeTarget,
        style: TextStyle(
            fontSize: 25.0,
            color: Theme.Colors.textTarget,
            fontWeight: FontWeight.bold),
      ),
    );
  }

  Widget _textOption(String texto) {
    return Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8, left: 10, right: 10),
      child: Text(
        texto,
        style: TextStyle(
            fontSize: 25.0,
            color: Theme.Colors.textOption,
            fontWeight: FontWeight.bold),
      ),
    );
  }

  Widget _eventButton(String text) {
    return
        // padding: const EdgeInsets.all(0.0),
        OutlineButton(
            borderSide: BorderSide(
              color: Colors.red, //Color of the border
              style: BorderStyle.solid, //Style of the border
              width: 0.8, //width of the border
            ),
            child: Padding(
              padding: EdgeInsets.only(top: 8, bottom: 8, left: 10, right: 10),
              child: Text(text,
                  //_isLoading? 'INICIANDO...' : '¡COMENZAR!',
                  style: TextStyle(
                      color: Colors.black,
                      fontFamily: "Poppins-Bold",
                      fontSize: 15,
                      letterSpacing: 1.0)),
            ),
            color: Theme.Colors.buttonComezar,
            // disabledColor: Theme.Colors.buttonComezar,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(20.0)),
            onPressed: () {
              load();
              i++;
              /*setState(() {
                //_loading();
                
              });*/
             // i++;
            });
  }
  Widget _eventButtonContinuar() {
    return
        // padding: const EdgeInsets.all(0.0),
        OutlineButton(
          
            borderSide: BorderSide(
              color: Colors.white, //Color of the border
              style: BorderStyle.solid, //Style of the border
              width: 2, //width of the border
            ),
    
              child: Text('CONTINUAR',
                  style: TextStyle(
                    
                      color: Colors.white,
                      fontFamily: "Poppins-Bold",
                      fontSize: 15,
                      letterSpacing: 2.0)),
            color: Theme.Colors.buttonComezar,
            
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(10.0)),
            onPressed: () {
              load();
              asignarAleatorio();
              i++;
            });
  }
  load() {
    final snackBar = SnackBar(
     // behavior: SnackBarBehavior.floating,
        duration: const Duration(milliseconds: 20),
        backgroundColor: Theme.Colors.loadCard,
        content: Container(
          height: MediaQuery.of(context).size.height ,
        ));
    Scaffold.of(context).showSnackBar(snackBar);
  }

  play() async {
    await audioPlayer.play(pathAudio == null ? '' : pathAudio);
    setState(() {
      playerState = PlayerState.playing;
    });
  }

   playPrasauej() async {
    // _isPlaying=true;
 _controller.reset();
   // _isPlaying=false;
    /*if (_isPlaying) ;
    else _controller.repeat();*/
    
       await audioPlayer.play(pathAudio == null ? '' : pathAudio);
    setState(() {
      playerState = PlayerState.playing;
      _isPlaying = !_isPlaying;
    });
  }


    Widget _carrousel() {

        return new Swiper(
          layout: SwiperLayout.CUSTOM,
          customLayoutOption: new CustomLayoutOption(
              startIndex: -1,
              stateCount: 3
          ).addRotate([
            -45.0/180,
            0.0,
            45.0/180
          ]).addTranslate([
            new Offset(-370.0, -40.0),
            new Offset(0.0, 0.0),
            new Offset(370.0, -40.0)
          ]),
          itemWidth: 300.0,
          itemHeight: 300.0,
          itemBuilder: (BuildContext context, int index) {
            
            return new Card(
              color: Theme.Colors.carruselBackground,
              child:Padding(
              padding: const EdgeInsets.all(30.0),
                child: new Center(
                  child: Container(
                          //child:_buildLessonItem(data[index]),
                          child:_buildLessonItem(),
                        ),
                ),
              )
            );
          },
          itemCount: (data == null ? 0 : cards.length),
          //key: UniqueKey(),
          scrollDirection: Axis.horizontal,
          //pagination: new SwiperPagination(),
          pagination: new SwiperPagination(
                margin: new EdgeInsets.fromLTRB(0.0, 250.0, 0.0, 30.0),
                builder: new DotSwiperPaginationBuilder(
                    color: Colors.black12,
                    activeColor: Colors.blue,
                    size: 20.0,
                    activeSize: 20.0)),
            
        //control: new SwiperControl(),
        );
    

  }

/* ============================================================================ */
  aleatorio() {
    var rng = new Random();
    int ran = rng.nextInt(5);
    switch (ran) {
      case 0:
        return '012';
      case 1:
        return '021';
      case 2:
        return '102';
      case 3:
        return '120';
      case 4:
        return '201';
      case 5:
        return '210';
    }
  }
  void asignarAleatorio(){
    String ind = aleatorio();
    a = int.parse(ind.substring(0, 1));
    b = int.parse(ind.substring(1, 2));
    c = int.parse(ind.substring(2, 3));
  }

  void onCards() async {
    setState(() {
      _isLoading = true;
    });
    /* Navigator.push(
        context,
        new MaterialPageRoute(
        builder: (context) => UnidadList())); */
    setState(() {
      _isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    // Call the getJSONData() method when the app initializes
    this.getJSONData();
    // this.getCardTinpal();
     _controller = AnimationController(
      vsync: this,
      lowerBound: 0.3,
      duration: Duration(seconds: 3),
    );
  }

  Widget _loading(){
    return  Padding(
      padding: EdgeInsets.only(top: 200, bottom: 0, left: 150, right: 0),
      child: new CircularProgressIndicator(),
    );
  }
}
