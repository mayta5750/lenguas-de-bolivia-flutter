import 'package:flutter/material.dart';
import 'package:tutorial_project/api/data.dart';
import 'package:cached_network_image/cached_network_image.dart';

import 'dart:async';
import 'package:tutorial_project/Theme.dart' as Theme;

class FinFragment extends StatefulWidget {
  FinFragment({Key key, this.description}) : super(key: key);
  final String description;
  @override
  _FinFragment createState() => _FinFragment();
}

enum PlayerState { stopped, playing, paused }

class _FinFragment extends State<FinFragment>
    with SingleTickerProviderStateMixin {


  Future<String> getJSONData() async {
    setState(() {
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    print('==============FIN=================');
    return _content();
  }
  Widget _content() {
    return Container(
        // height: 200,
        height: MediaQuery.of(context).size.height - 60,
        width: MediaQuery.of(context).size.width,
        child: Card(
          elevation: 3,
          color: Theme.Colors.bodyCard_1,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(8.0),
            ),
          ),
          margin: const EdgeInsets.all(20),
          child:SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              //mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Padding(
                padding:EdgeInsets.only(top: 30, bottom: 15, left: 0, right: 0),
                child: Center(
                  child: _leccionCompletada("¡"+widget.description+" Completada!"),
                )
              ),
              
              _imagen(),
              _textFelicidades("¡Felicidades!"),
              _textCompletado("Has completado la lección"),
              Padding(
                padding:EdgeInsets.only(top: 30, bottom: 15, left: 0, right: 0),
                child: Center(
                  child: _eventButton(),
                )
              ),
            ],
          ),
        ),
      )
    );
  }
  Widget _textFelicidades(String text) {
    return Padding(
        padding: EdgeInsets.only(top: 8, bottom: 8, left: 10, right: 10),
        child: Text(
          text,
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 28.0,
              color: Theme.Colors.textSource,
              fontWeight: FontWeight.bold),
        )
    );
  }
    Widget _textCompletado(String text) {
    return Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8, left: 10, right: 10),
      child: Text(
        text,
        style: TextStyle(
            fontSize: 25.0,
            color: Theme.Colors.textTarget,
            fontWeight: FontWeight.bold),
      ),
    );
  }
  Widget _leccionCompletada(String texto) {
    return Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8, left: 10, right: 10),
      child: Text(
        texto,
        textAlign: TextAlign.center,
        style: TextStyle(
            fontSize: 22.0,
            color: Theme.Colors.textOption,
            fontWeight: FontWeight.bold),
      ),
    );
  }
  Widget _imagen() {
    return Image(
      image: AssetImage('assets/icons/final.png'),
      width: 200,
      height: 200,
    );
  }




  Widget _eventButton() {
    return
        // padding: const EdgeInsets.all(0.0),
        OutlineButton(
            borderSide: BorderSide(
              color: Theme.Colors.buttonNext, //Color of the border
              style: BorderStyle.solid, //Style of the border
              width: 2, //width of the border
            ),
            child: Padding(
              padding: EdgeInsets.only(top: 8, bottom: 8, left: 10, right: 10),
              child: Text('CONTINUAR',
                  //_isLoading? 'INICIANDO...' : '¡COMENZAR!',
                  style: TextStyle(
                      color: Colors.black,
                      fontFamily: "Poppins-Bold",
                      fontSize: 15,
                      letterSpacing: 1.0)),
            ),
            color: Theme.Colors.buttonComezar,
            // disabledColor: Theme.Colors.buttonComezar,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(20.0)),
            onPressed: () {
              // return(new TinpalFragment(title:'TARJETA INFORMATIVA',id_key_item:item['id_key_item']));
              print('==========push FIN==========');
              Navigator.pop(context);
            }
        );
  }
  Widget _loading(){
    return  Padding(
      padding: EdgeInsets.only(top: 200, bottom: 0, left: 0, right: 0),
      child: new CircularProgressIndicator(),
    );
  }
  @override
  void initState() {
    super.initState();
    // Call the getJSONData() method when the app initializes
    this.getJSONData();
    //initAudioPlayer();
    // this.getCardTinpal();
  }
}
