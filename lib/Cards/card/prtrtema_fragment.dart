import 'package:flutter/material.dart';
import 'package:tutorial_project/api/data.dart';
import 'package:cached_network_image/cached_network_image.dart';

import 'dart:async';
import 'dart:math';
import 'package:tutorial_project/Theme.dart' as Theme;
import 'package:tutorial_project/Models/WordsImage.dart';
import 'package:tutorial_project/Models/WordsAudio.dart';
import 'package:tutorial_project/Models/DictionaryWords.dart';
import 'package:flutter_svg/flutter_svg.dart';
//import 'package:audioplayer/audioplayer.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:tutorial_project/Cards/card/card.dart';

class PrtrtemaFragment extends StatefulWidget {
  PrtrtemaFragment({Key key, this.cards, this.index}) : super(key: key);
  var cards;
  int index;
  @override
  _PrtrtemaFragment createState() => _PrtrtemaFragment();
}

enum PlayerState { stopped, playing, paused }

class _PrtrtemaFragment extends State<PrtrtemaFragment>
    with SingleTickerProviderStateMixin {
  WordsImage wordImage;
  WordsAudio wordAudio;
  DictionaryWords wordDictionaryWord;
  DictionaryWords wordDictionaryWordOption_1;
  DictionaryWords wordDictionaryWordOption_2;

  String graphemeTarget;
  String graphemeSource;
  String graphemeSourceOption_1;
  String graphemeSourceOption_2;
  String pathImagen;
  String pathAudio;

  String indice;

  String texto='';

  int a, b, c;

  List lista;

  /*AudioPlayer audioPlayer = new AudioPlayer();
  AudioPlayer audioPlugin = new AudioPlayer();
  PlayerState playerState = PlayerState.stopped;*/

  AudioPlayer _audioPlayer;
  PlayerState _playerState = PlayerState.stopped;
  PlayerMode mode;

  TextEditingController editingController;
  Future<String> getJSONData() async {
    wordDictionaryWord = await DataApi().getDictionaryWords(widget.cards[widget.index]['id_key_item']);
    wordImage = await DataApi().getWordImage(widget.cards[widget.index]['id_key_item']);
    wordAudio =  await DataApi().getWordAudio(widget.cards[widget.index]['id_key_item']);
    setState(() {
      graphemeTarget = wordDictionaryWord.getGraphemeTarget();
      graphemeSource = wordDictionaryWord.getGraphemeSource();
      if(wordImage!=null)
        pathImagen = wordImage.getPath();
      pathAudio = wordAudio.getPath();
      listaAleatoria();
      editingController = TextEditingController(text:texto);
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    print('==============Prtrtema=================');
    return Padding(
      padding: EdgeInsets.only(top: 0, bottom: 0, left: 0, right: 0),
      child: wordDictionaryWord == null
          ? _loading()
          : _prtrtema(),
    );
    /*return Padding(
      padding: EdgeInsets.only(top: 0, bottom: 0, left: 0, right: 0),
      child: _prtrtema(),
    );*/
  }

  Widget _prtrtema() {
    return Container(
      // height: 200,
      height: MediaQuery.of(context).size.height - 60,
      width: MediaQuery.of(context).size.width,
      child: Card(
        elevation: 3,
        color: Theme.Colors.bodyCard_1,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(8.0),
          ),
        ),
        margin: const EdgeInsets.all(20),
        child: SingleChildScrollView(
          child:Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 20, bottom: 10, left: 0, right: 0),
                  child:wordImage==null?_textOption("Traduce :"):_graphemeTarget(),
              ),
              _imagenJpg(),
              _textIn(),
              Padding(
                padding: EdgeInsets.only(top: 0, bottom: 15, left: 0, right: 0),
                child: Center(child:_buttonFila(),)
              ),
              _buttonBorrar(),
              _eventButton(),
                
            ],
          ),
        ),
      ),
    );
  }

  Widget _textOption(String texto) {
   // listaAleatoria();
    return Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8, left: 10, right: 10),
      child: Text(
        texto,
        style: TextStyle(
            fontSize: 22.0,
            color: Theme.Colors.textOption,
            fontWeight: FontWeight.bold),
      ),
    );
  }

  Widget _graphemeTarget() {
    return Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8, left: 10, right: 10),
      child: Text(
        graphemeTarget == null ? '' : graphemeTarget,
        textAlign: TextAlign.center,
        style: TextStyle(
            fontSize: 23.0,
            color: Theme.Colors.textTarget,
            fontWeight: FontWeight.bold),
      ),
    );
  }

  Widget _imagenJpg() {
    print(pathImagen);
    if(wordImage==null){
    return Container(
      //  margin: const EdgeInsets.all(15.0),
        decoration: BoxDecoration(
          border: Border.all(
            color: Colors.green,
          ),
          borderRadius: BorderRadius.all(Radius.circular(9.0)),
        ),
        width: 150,
        height: 150,
        child: Center(
            child: Align(
          alignment: Alignment.center,
          child: Text(
        graphemeTarget == null ? '' : graphemeTarget,
        textAlign: TextAlign.center,
        style: TextStyle(
            fontSize: 15.0,
            color: Theme.Colors.textTarget,
            fontWeight: FontWeight.bold
          ),
      ),
        )
      )
    );
    }else{
    if(pathImagen.substring(pathImagen.length-3)=='svg'){
    return new SvgPicture.network(
      pathImagen,
      semanticsLabel: 'Feed button',
      height: 150.0,
      width: 150.00,
      fit: BoxFit.contain,
    );
    } else {
      return new CachedNetworkImage(                
      imageUrl: pathImagen,
      height: 150.0,
      width: 150.00,
      );
    }            
    }          
                    
  }

  Widget _textIn() {
    return Container(
      width: MediaQuery.of(context).size.height - 60,
        margin: const EdgeInsets.only(top: 15, bottom: 0, left: 15, right: 15),
        decoration: BoxDecoration(
          border: Border.all(
            color: Colors.green,
          ),
          borderRadius: BorderRadius.all(Radius.circular(25.0)),
        ),
            child:Padding(
        padding: EdgeInsets.only(top: 10, bottom: 10, left: 10, right: 10),
        child: Text(
          texto,
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 20.0,
              color: Theme.Colors.textLec,
            ),
        ),
      ),
    );
  }

  Widget _buttonFila(){
    return  Center(
        //height: 50.0,
       // width: 100,
        child: GridView.builder(
          physics: const NeverScrollableScrollPhysics(),
          gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount:lista.length<6?lista.length:6
          ),
          shrinkWrap: true,
          itemCount: lista == null ? 0 : (lista.length),
          itemBuilder: (BuildContext context, int i){
            /*if(lista.length==i)
              return _buttonBorrar();
            else*/
              return _buttonSelect(lista[i]);
          }
        ),
      );
  }

  Widget _buttonSelect(String text) {
    return Padding(
        padding: const EdgeInsets.all(5.0),
        child: SizedBox(
         // height: 30.0,
          child: FlatButton(
            child: Text(text,
                //_isLoading? 'INICIANDO...' : 'INICIAR SESIÓN',
                textDirection: TextDirection.ltr,
                style: TextStyle(
                    color: Colors.white,
                    fontFamily: "Poppins-Bold",
                    fontSize: 15,)),
            color: Theme.Colors.buttonIniciar,
            disabledColor: Theme.Colors.buttonIniciando,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(5.0)),
            onPressed:() async{
            print(texto);
            setState(() {
              texto=texto+text;
              });
            if (texto == graphemeSource) {
            await _audioPlayer.play(pathAudio, isLocal: false);
            setState(() => _playerState = PlayerState.playing);
            _showMsg('Tu respuesta es correcta', '');
            const twentyMillis = const Duration(seconds: 2);
            new Timer(twentyMillis, () => Navigator.pop(context));
            new Timer(
                twentyMillis,
                () => Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => CollectionCards(
                        cards: widget.cards, index: (widget.index) + 1))));
            }
            }
          ),
        ));
  }
  Widget _buttonBorrar() {
    return Padding(
        padding: const EdgeInsets.all(1.0),
        child:OutlineButton(
            borderSide: BorderSide(
              color: Theme.Colors.buttonIniciando, //Color of the border
              style: BorderStyle.solid, //Style of the border
              width: 2, //width of the border
            ),
            child: Icon(Icons.arrow_back),
            color: Theme.Colors.buttonComezar,
            shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(10.0)),
            onPressed: () {
            setState(() {
              texto=texto.substring(0,texto.length-1);
            });
            }));
  }
  Widget _buttonPrtrtema(String text) {
    return Center(
        child: SizedBox(
      width: MediaQuery.of(context).size.width - 80,
      child: new RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(10.0),
          // side: BorderSide(color: Colors.red)
        ),
        child: Text(text,
            style: TextStyle(
                color: Colors.white,
                fontFamily: "Poppins-Bold",
                fontSize: 17,
                letterSpacing: 1.0)),
        elevation: 5.0,
        color: Theme.Colors.collectionButtons,
        onPressed: () async {
          if (text == graphemeSource) {
            await _audioPlayer.play(pathAudio, isLocal: false);
            setState(() => _playerState = PlayerState.playing);
            _showMsg('Tu respuesta es correcta', '');
          } else {
            await _audioPlayer.play('https://back.tecnicos.lenguas.org.bo/raw/wrong.mp3', isLocal: false);
            setState(() => _playerState = PlayerState.playing);   
            _showMsg('La respuesta correcta es :', graphemeSource);
          }
          const twentyMillis = const Duration(seconds: 2);
          new Timer(twentyMillis, () => Navigator.pop(context));
          new Timer(
              twentyMillis,
              () => Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => CollectionCards(
                      cards: widget.cards, index: (widget.index) + 1))));
        },
      ),
    ));
  }

  _showMsg(msg, msg2) {
    //
    final snackBar = SnackBar(
        duration: const Duration(milliseconds: 1000),
        backgroundColor: msg2 == ''
            ? Theme.Colors.showMsgCorrecto
            : Theme.Colors.showMsgInCorrecto,
        content: Container(
            height: 150.0,
            width: MediaQuery.of(context).size.width + 20,
            child: Column(
              children: <Widget>[
                msg2 == ''
                    ? Image(
                        image: AssetImage('assets/icons/bien.png'),
                        width: 50,
                        height: 50,
                        fit: BoxFit.cover)
                    : Image(
                        image: AssetImage('assets/icons/mal.png'),
                        fit: BoxFit.cover),
                Text(
                  msg,
                  style: TextStyle(
                      fontSize: 22.0,
                      color: Colors.white,
                      letterSpacing: 2.0,
                      fontWeight: FontWeight.bold),
                ),
                msg2 == ''
                    ? Text(msg2, style: TextStyle(fontSize: 0))
                    : Text(
                        msg2,
                        style: TextStyle(
                            fontSize: 24.0,
                            color: Colors.white,
                            letterSpacing: 1.0,
                            fontWeight: FontWeight.bold),
                      ),
                //_eventButtonContinuar(),
              ],
            )));
    Scaffold.of(context).showSnackBar(snackBar);
  }
  Widget _loading(){
    return  Padding(
      padding: EdgeInsets.only(top: 200, bottom: 0, left: 0, right: 0),
      /*child:Image(
          image: AssetImage('assets/ops/loading.png'),
          width: 200,
          height: 200,
          fit: BoxFit.cover
      ),*/
      child: new CircularProgressIndicator(),
    );
  }
    Widget _eventButton() {
    return
        // padding: const EdgeInsets.all(0.0),
        OutlineButton(
            borderSide: BorderSide(
              color: Theme.Colors.buttonNext, //Color of the border
              style: BorderStyle.solid, //Style of the border
              width: 2, //width of the border
            ),
            child: Padding(
              padding: EdgeInsets.only(top: 8, bottom: 8, left: 10, right: 10),
              child: Text('COMPROBAR',
                  //_isLoading? 'INICIANDO...' : '¡COMENZAR!',
                  style: TextStyle(
                      color: Colors.black,
                      fontFamily: "Poppins-Bold",
                      fontSize: 15,
                      letterSpacing: 1.0)),
            ),
            color: Theme.Colors.buttonComezar,
            // disabledColor: Theme.Colors.buttonComezar,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(20.0)),
            onPressed: () async {
          if (texto == graphemeSource) {
            _showMsg('Tu respuesta es correcta', '');
            await _audioPlayer.play(pathAudio, isLocal: false);
            setState(() => _playerState = PlayerState.playing);
          } else {
            await _audioPlayer.play('https://back.tecnicos.lenguas.org.bo/raw/wrong.mp3', isLocal: false);
            setState(() => _playerState = PlayerState.playing);  
           _showMsg('La respuesta correcta es :', graphemeSource);
          }
          const twentyMillis = const Duration(seconds: 2);
          new Timer(twentyMillis, () => Navigator.pop(context));
          new Timer(
              twentyMillis,
              () => Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => CollectionCards(
                      cards: widget.cards, index: (widget.index) + 1))));
        }
    );
  }

  listaAleatoria() {
    List aux =[];
    for (int i = 0; i < graphemeSource.length; i++) {
      aux.add(graphemeSource.substring(i,i+1));
    }
    print(aux);
    String cad = '';
    for (var i = aux.length; i >= 1; i--) {
      var rng = new Random();
      int ran = rng.nextInt(i);
      cad = aux[ran] + '&' + cad;
      aux.remove(aux[ran]);
    }
  lista = cad.split('&');
    lista.remove('');
    lista.remove('');
    print(lista);
  }
  void _initAudioPlayer() {
    _audioPlayer = AudioPlayer(mode: mode);
  }
  @override
  void initState() {
    super.initState();
    // Call the getJSONData() method when the app initializes
    this.getJSONData();
    _initAudioPlayer ();
  }
}
