import 'package:flutter/material.dart';
import 'package:tutorial_project/api/data.dart';

import 'dart:async';
import 'dart:math';
import 'package:tutorial_project/Theme.dart' as Theme;
import 'package:tutorial_project/Models/WordsImage.dart';
import 'package:tutorial_project/Models/WordsAudio.dart';
import 'package:tutorial_project/Models/DictionaryWords.dart';
//import 'package:audioplayer/audioplayer.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:tutorial_project/Cards/card/card.dart';

class PrasauejFragment extends StatefulWidget {
  PrasauejFragment({Key key, this.cards, this.index}) : super(key: key);
  var cards;
  int index;
  @override
  _PrasauejFragment createState() => _PrasauejFragment();
}

enum PlayerState { stopped, playing, paused }

class _PrasauejFragment extends State<PrasauejFragment>
    with SingleTickerProviderStateMixin {
  WordsImage wordImage;
  WordsAudio wordAudio;
  DictionaryWords wordDictionaryWord;
  DictionaryWords wordDictionaryWordOption_1;

  String graphemeTarget;
  String graphemeSource;
  String graphemeSourceOption_1;
  String pathImagen;
  String pathAudio;

  String indice;

  int a, b, c;

  List list;

  /*AudioPlayer audioPlayer = new AudioPlayer();
  AudioPlayer audioPlugin = new AudioPlayer();
  PlayerState playerState = PlayerState.stopped;*/
  AudioPlayer _audioPlayer;
  PlayerState _playerState = PlayerState.stopped;
  PlayerMode mode;

  Future<String> getJSONData() async {
    // print(widget.cards);
    wordDictionaryWord = await DataApi().getDictionaryWords(widget.cards[widget.index]['id_key_item']);
    wordDictionaryWordOption_1 = await DataApi().getDictionaryWords(widget.cards[widget.index]['option_dict_word_1']);
    wordImage = await DataApi().getWordImage(widget.cards[widget.index]['id_key_item']);
    wordAudio = await DataApi().getWordAudio(widget.cards[widget.index]['id_key_item']);
    setState(() {
      graphemeTarget = wordDictionaryWord.getGraphemeTarget();
      graphemeSource = wordDictionaryWord.getGraphemeSource();
      graphemeSourceOption_1 = wordDictionaryWordOption_1.getGraphemeSource();
      pathImagen = wordImage.getPath();
      pathAudio = wordAudio.getPath();

      indice = aleatorio();
      a = int.parse(indice.substring(0, 1));
      b = int.parse(indice.substring(1, 2));
      list = [
        graphemeSource,
        graphemeSourceOption_1,
      ];
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    print('==============Prasauej=================');
    /*return Padding(
      padding: EdgeInsets.only(top: 0, bottom: 0, left: 0, right: 0),
      child: graphemeSourceOption_1 == null
          ? _loading()
          : _prasauej(),
    );*/
    return Padding(
      padding: EdgeInsets.only(top: 0, bottom: 0, left: 0, right: 0),
      child: _prasauej(),
    );
  }

  Widget _prasauej() {
    return Container(
      // height: 200,
      height: MediaQuery.of(context).size.height - 60,
      width: MediaQuery.of(context).size.width,
      child: Card(
        elevation: 3,
        color: Theme.Colors.bodyCard_1,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(8.0),
          ),
        ),
        margin: const EdgeInsets.all(20),
        child: Stack(
          children: <Widget>[
            Positioned.fill(
              top: 30,
              child: Align(
                alignment: Alignment.topCenter,
                child: _textOption("¿Cuál opción corresponde al audio?"),
              ),
            ),
            Positioned.fill(
              top: 160,
              child: Align(
                alignment: Alignment.topCenter,
                child: FloatingActionButton(
                  onPressed: _play,
                  tooltip: 'Play',
                  child: const Icon(Icons.play_arrow),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 0, bottom: 25, left: 0, right: 0),
              child: _duoPalabra(),
            ),
          ],
        ),
      ),
    );
  }

  Widget _textOption(String texto) {
    return Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8, left: 10, right: 10),
      child: Text(
        texto,
        textAlign: TextAlign.center,
        style: TextStyle(
            fontSize: 22.0,
            color: Theme.Colors.textOption,
            fontWeight: FontWeight.bold),
      ),
    );
  }

  Widget _duoPalabra() {
    List aux = [
      wordDictionaryWord == null ? '' : graphemeSource,
      wordDictionaryWordOption_1 == null ? '' : graphemeSourceOption_1,
    ];
    return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        //mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Row(
            children: <Widget>[
              Column(
               children: <Widget>[
              _palabra(aux[a]),
              _button(aux[a])
              ]
            ),
              Column(
               children: <Widget>[
              _palabra(aux[b]),
              _button(aux[b])
              ]
            ),
            ],
          )
        ]
    );
  }

  Widget _palabra(String palabra) {
    return Container(
        margin: const EdgeInsets.all(15.0),
        decoration: BoxDecoration(
          border: Border.all(
            color: Theme.Colors.buttonNext,
          ),
          borderRadius: BorderRadius.all(
              Radius.circular(9.0) //                 <--- border radius here
              ),
        ),
        width: 130,
        height: 130,
        child: Center(
          child: Text(
            palabra,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 20.0,
                color: Theme.Colors.colorDuo,
                letterSpacing: 1.0,
                fontWeight: FontWeight.bold),
          ),
        ));
  }
    Widget _button(String text) {
    return SizedBox(
   //   width: 80, // specific value
      child: new RaisedButton(
        shape: RoundedRectangleBorder(
        borderRadius: new BorderRadius.circular(10.0),
       // side: BorderSide(color: Colors.red)
        ),
        child: Text('SELECCIONAR',
            style: TextStyle(
                color: Colors.white,
                fontFamily: "Poppins-Bold",
                fontSize: 15,
                letterSpacing: 1.0)),
        elevation: 5.0,
        color: Theme.Colors.collectionButtons,
        onPressed: () async{
          if (text == graphemeSource) {
            await _audioPlayer.play(pathAudio, isLocal: false);
            setState(() => _playerState = PlayerState.playing);
            _showMsg('Tu respuesta es correcta', '');
          } else {
            await _audioPlayer.play('https://back.tecnicos.lenguas.org.bo/raw/wrong.mp3', isLocal: false);
            setState(() => _playerState = PlayerState.playing);
            _showMsg('La respuesta correcta es :', graphemeSource);
          }
          const twentyMillis = const Duration(seconds:2);
            new Timer(twentyMillis, () =>
  Navigator.pop(context));
  new Timer(twentyMillis, () =>
  Navigator.of(context).push(MaterialPageRoute(builder: (context) => 
  CollectionCards(cards:widget.cards,index:(widget.index)+1))));
        },
      ),
    );
  }
  _showMsg(msg, msg2) {
    //
    final snackBar = SnackBar(
        duration: const Duration(milliseconds: 1000),

        backgroundColor: msg2 == ''
            ? Theme.Colors.showMsgCorrecto
            : Theme.Colors.showMsgInCorrecto,
        content: Container(
            height: 150.0,
            width: MediaQuery.of(context).size.width + 20,
            child: Column(
              children: <Widget>[
                msg2 == ''
                    ? Image(
                        image: AssetImage('assets/icons/bien.png'),
                        width: 50,
                        height: 50,
                        fit: BoxFit.cover)
                    : Image(
                        image: AssetImage('assets/icons/mal.png'),
                        fit: BoxFit.cover),
                Text(
                  msg,
                  style: TextStyle(
                      fontSize: 22.0,
                      color: Colors.white,
                      letterSpacing: 2.0,
                      fontWeight: FontWeight.bold),
                ),
                msg2 == ''
                    ? Text(msg2, style: TextStyle(fontSize: 0))
                    : Text(
                        msg2,
                        style: TextStyle(
                            fontSize: 24.0,
                            color: Colors.white,
                            letterSpacing: 1.0,
                            fontWeight: FontWeight.bold),
                      ),
                //_eventButtonContinuar(),
              ],
            )));
    Scaffold.of(context).showSnackBar(snackBar);
  }
  aleatorio() {
    var rng = new Random();
    int ran = rng.nextInt(1);
    switch (ran) {
      case 0:
        return '01';
      case 1:
        return '10';
    }
  }
  Widget _loading(){
    return  Padding(
      padding: EdgeInsets.only(top: 200, bottom: 0, left: 0, right: 0),
      child: new CircularProgressIndicator(),
    );
  }
  void _initAudioPlayer() {
    _audioPlayer = AudioPlayer(mode: mode);
  }
  Future<int> _play() async {
    final result =
        await _audioPlayer.play(pathAudio, isLocal: false);
    if (result == 1) setState(() => _playerState = PlayerState.playing);
    return result;
  }
  @override
  void initState() {
    super.initState();
    // Call the getJSONData() method when the app initializes
    this.getJSONData();
    _initAudioPlayer();
  }
}
