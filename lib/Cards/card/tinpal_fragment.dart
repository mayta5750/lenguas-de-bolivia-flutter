import 'package:flutter/material.dart';
import 'package:tutorial_project/api/data.dart';
import 'package:cached_network_image/cached_network_image.dart';

import 'dart:async';
import 'package:tutorial_project/Theme.dart' as Theme;
import 'package:tutorial_project/Models/WordsImage.dart';
import 'package:tutorial_project/Models/WordsAudio.dart';
import 'package:tutorial_project/Models/DictionaryWords.dart';
import 'package:flutter_svg/flutter_svg.dart';
//import 'package:audioplayer/audioplayer.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:tutorial_project/Cards/card/card.dart';

class TinpalFragment extends StatefulWidget {
  TinpalFragment({Key key, this.cards, this.index}) : super(key: key);
  // final String title;
  //final String id_key_item;
  //final String id_key_item;
  var cards;
  int index;
  @override
  _TinpalFragment createState() => _TinpalFragment();
}

enum PlayerState { stopped, playing, paused }

class _TinpalFragment extends State<TinpalFragment>
    with SingleTickerProviderStateMixin {
  WordsImage wordImage;
  WordsAudio wordAudio;
  DictionaryWords wordDictionaryWord;

  String graphemeTarget;
  String graphemeSource;
  String pathImagen;
  String pathAudio;
  /*AudioPlayer audioPlayer = new AudioPlayer();
  AudioPlayer audioPlugin = new AudioPlayer();
  PlayerState playerState = PlayerState.stopped;*/

  AudioPlayer _audioPlayer;
  PlayerState _playerState = PlayerState.stopped;
  PlayerMode mode;

  Future<String> getJSONData() async {
    print(widget.cards);

    wordImage = await DataApi().getWordImage(widget.cards[widget.index]['id_key_item']);
    wordAudio = await DataApi().getWordAudio(widget.cards[widget.index]['id_key_item']);
    wordDictionaryWord = await DataApi().getDictionaryWords(widget.cards[widget.index]['id_key_item']);
    setState(() {
      graphemeTarget = wordDictionaryWord.getGraphemeTarget();
      graphemeSource = wordDictionaryWord.getGraphemeSource();
      if(wordImage!=null)
        pathImagen = wordImage.getPath();
      pathAudio = wordAudio.getPath();
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    print('==============TINPAL=================');
    //return wordDictionaryWord==null?_loading():_content();
    return _content();
  }
  Widget _content() {
    return Container(
        // height: 200,
        height: MediaQuery.of(context).size.height - 60,
        width: MediaQuery.of(context).size.width,
        child: Card(
          elevation: 3,
          color: Theme.Colors.bodyCard_1,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(8.0),
            ),
          ),
          margin: const EdgeInsets.all(20),
          child: SingleChildScrollView(
            child:Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                      padding:EdgeInsets.only(top: 20, bottom: 5, left: 5, right: 5),
                      child: Center(
                      child:wordImage==null?_textOption(""):_graphemeSource(),
                      ), 
              ),
              Stack(
                children: <Widget>[
                  Padding(
                    padding:EdgeInsets.only(top: 5, bottom: 15, left: 5, right: 5),
                    child: Center(
                      child: _imagenJpg(),
                    ),
                  ),
                  Positioned.fill(
                    right: 15,
                    child: Align(
                      alignment: Alignment.bottomRight,
                      child: 
                        FloatingActionButton(
                        onPressed: _play,
                        tooltip: 'Play',
                        child: const Icon(Icons.play_arrow),
                        ),
                      ),
                  ),
               ],
              ),
              Padding(
                padding:EdgeInsets.only(top: 5, bottom: 15, left: 5, right: 5),
                child: Center(
                  child:_graphemeTarget(),
                ),
              ),
              Padding(
                padding:EdgeInsets.only(top: 5, bottom: 15, left: 5, right: 5),
                child: Center(
                  child: _eventButton(),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _graphemeSource() {
    return Padding(
        padding: EdgeInsets.only(top: 8, bottom: 8, left: 10, right: 10),
        child: Text(
          graphemeSource == null ? '' : graphemeSource,
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 28.0,
              color: Theme.Colors.textSource,
              fontWeight: FontWeight.bold),
        )
    );
  }
  Widget _textOption(String texto) {
    return Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8, left: 10, right: 10),
      child: Text(
        texto,
        textAlign: TextAlign.center,
        style: TextStyle(
            fontSize: 22.0,
            color: Theme.Colors.textOption,
            fontWeight: FontWeight.bold),
      ),
    );
  }
  Widget _imagen() {
    return new SvgPicture.network(
      pathImagen == null
          ? 'http://web.oei.bo/dclo_public/img_out_L/L_ay_240518/5/1670.svg'
          : pathImagen,
      semanticsLabel: 'Feed button',
      height: 250.0,
      width: 250.00,
      fit: BoxFit.contain,
      // placeholderBuilder: (context,) => Icon(Icons.error),
    );
  }

  Widget _imagenJpg() {
    print(pathImagen);
    if(wordImage==null){
    return Container(
        margin: const EdgeInsets.all(15.0),
        decoration: BoxDecoration(
          border: Border.all(
            color: Colors.green,
          ),
          borderRadius: BorderRadius.all(Radius.circular(9.0)),
        ),
        width: 250,
        height: 250,
        child: Center(
            child: Align(
          alignment: Alignment.center,
          child: _graphemeSource()
        )
      )
    );
    }else{
    if(pathImagen.substring(pathImagen.length-3)=='svg'){
    return new SvgPicture.network(
      pathImagen,
      semanticsLabel: 'Feed button',
      height: 250.0,
      width: 250.0,
      fit: BoxFit.contain,
      // placeholderBuilder: (context,) => Icon(Icons.error),
    );
    } else {
      return new CachedNetworkImage(                
        imageUrl: pathImagen,
        height: 250.0,
        width: 250.0,
      );
    }            
    }          
                    
  }

  Widget _graphemeTarget() {
    return Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8, left: 10, right: 10),
      child: Text(
        graphemeTarget == null ? '' : graphemeTarget,
        textAlign: TextAlign.center,
        style: TextStyle(
            fontSize: 25.0,
            color: Theme.Colors.textTarget,
            fontWeight: FontWeight.bold),
      ),
    );
  }

  Widget _eventButton() {
    return
        // padding: const EdgeInsets.all(0.0),
        OutlineButton(
            borderSide: BorderSide(
              color: Theme.Colors.buttonNext, //Color of the border
              style: BorderStyle.solid, //Style of the border
              width: 2, //width of the border
            ),
            child: Padding(
              padding: EdgeInsets.only(top: 8, bottom: 8, left: 10, right: 10),
              child: Text('SIGUIENTE',
                  //_isLoading? 'INICIANDO...' : '¡COMENZAR!',
                  style: TextStyle(
                      color: Colors.black,
                      fontFamily: "Poppins-Bold",
                      fontSize: 15,
                      letterSpacing: 1.0)),
            ),
            color: Theme.Colors.buttonComezar,
            // disabledColor: Theme.Colors.buttonComezar,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(20.0)),
            onPressed: () {
              // return(new TinpalFragment(title:'TARJETA INFORMATIVA',id_key_item:item['id_key_item']));
              print('==========push TINPAL==========');
              Navigator.pop(context);
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => CollectionCards(
                      cards: widget.cards, index: (widget.index) + 1)));
            });
  }
  Widget _loading(){
    return  Padding(
      padding: EdgeInsets.only(top: 200, bottom: 0, left: 0, right: 0),
      child: new CircularProgressIndicator(),
    );
  }
 /* play() async {
    await audioPlayer.play(pathAudio == null ? '' : pathAudio);
    setState(() {
      playerState = PlayerState.playing;
    });
  }*/
  void _initAudioPlayer() {
    _audioPlayer = AudioPlayer(mode: mode);
  }
  Future<int> _play() async {
    final result =
        await _audioPlayer.play(pathAudio, isLocal: false);
    if (result == 1) setState(() => _playerState = PlayerState.playing);
    return result;
  }

  @override
  void initState() {
    super.initState();
    // Call the getJSONData() method when the app initializes
    this.getJSONData();
    _initAudioPlayer ();
    //initAudioPlayer();
    // this.getCardTinpal();
  }
}
