import 'package:flutter/material.dart';
import 'package:tutorial_project/api/data.dart';

import 'dart:async';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tutorial_project/Cards/CardsScreen.dart';
import 'package:tutorial_project/api/api.dart';
import 'package:tutorial_project/UnidadList/UnidadListScreen.dart';

import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:tutorial_project/Theme.dart' as Theme;
import 'package:percent_indicator/percent_indicator.dart';

import 'package:tutorial_project/Models/WordsImage.dart';
import 'package:tutorial_project/Models/WordsAudio.dart';
import 'package:tutorial_project/Models/DictionaryWords.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:audioplayer/audioplayer.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:tutorial_project/Cards/card/tinpal_fragment.dart';
import 'package:tutorial_project/Cards/card/prseiama_fragment.dart';
import 'package:tutorial_project/Cards/card/prasauej_fragment.dart';
import 'package:tutorial_project/Cards/card/prmau_fragment.dart';
import 'package:tutorial_project/Cards/card/prarfra_fragment.dart';
import 'package:tutorial_project/Cards/card/prtrtema_fragment.dart';
import 'package:tutorial_project/Cards/card/prmiiama_fragment.dart';
import 'package:tutorial_project/Cards/card/fin_fragment.dart';



class CollectionCards extends StatefulWidget {
  CollectionCards({Key key, this.cards, this.index})
      : super(key: key);
  var cards;
  int index;
  @override
  _CollectionCards createState() => _CollectionCards();
}
enum PlayerState { stopped, playing, paused }
class _CollectionCards extends State<CollectionCards>
  with SingleTickerProviderStateMixin {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    print('==============card=================');
      return Scaffold(
      backgroundColor: Theme.Colors.bodyBackground,
      body:
      Padding(
        padding: EdgeInsets.only(top: 30, bottom: 0, left: 0, right: 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            _progresBar(),
            _buildLessonItem(widget.cards[widget.index]),
          ],
        )
        )
    );
  }

  Widget _progresBar() {
    return Padding(
      padding: EdgeInsets.only(left: 15, right: 15, top: 15),
      child: new LinearPercentIndicator(
        width: MediaQuery.of(context).size.width - 30,
        animation: true,
        lineHeight: 10.0,
        animationDuration: 200,
        percent: widget.index/widget.cards.length,
        // center: Text("90.0%"),
        linearStrokeCap: LinearStrokeCap.roundAll,
        progressColor: Colors.greenAccent,
      ),
    );
  }


    Widget _buildLessonItem(dynamic item) {
    /* ===================tarjeta================ */
    String card = item['cat_card_type'] == null ? '' : item['cat_card_type'];
    print('YYYYYYYYYYYYYYYYYYYYYYYYY');
    print(item);
   print('aasdasdasd');
    switch (card) {
      case 'TINPAL':
       // Navigator.pop(context);
        return(new TinpalFragment(cards:widget.cards,index:(widget.index)));
        //return _tinpal();
      case 'PRSEIAMA':
        return(new PrseiamaFragment(cards:widget.cards,index:(widget.index)));
      case "PRASAUEJ":
        return(new PrasauejFragment(cards:widget.cards,index:(widget.index)));
      case "PRMAU":
        return(new PrmauFragment(cards:widget.cards,index:(widget.index)));
      case "PRARFRA":
        return(new PrarfraFragment(cards:widget.cards,index:(widget.index)));
      case "PRTRTEMA":
        return(new PrtrtemaFragment(cards:widget.cards,index:(widget.index)));
      case "PRMIIAMA":
        return(new PrmiiamaFragment(cards:widget.cards,index:(widget.index)));
      default:
        return(new FinFragment(description: item['cat_card_type']));
    }
    return Container();
  }

  @override
  void initState() {
    super.initState();
    // Call the getJSONData() method when the app initializes
   // this.getJSONData();
    //initAudioPlayer();
    // this.getCardTinpal();
  }
}

