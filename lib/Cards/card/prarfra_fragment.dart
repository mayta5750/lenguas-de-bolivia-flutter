import 'package:flutter/material.dart';
import 'package:tutorial_project/api/data.dart';

import 'dart:async';
import 'dart:math';
import 'package:tutorial_project/Theme.dart' as Theme;
import 'package:tutorial_project/Models/WordsImage.dart';
import 'package:tutorial_project/Models/WordsAudio.dart';
import 'package:tutorial_project/Models/DictionaryWords.dart';
//import 'package:audioplayer/audioplayer.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:tutorial_project/Cards/card/card.dart';

class PrarfraFragment extends StatefulWidget {
  PrarfraFragment({Key key, this.cards, this.index}) : super(key: key);
  var cards;
  int index;
  @override
  _PrarfraFragment createState() => _PrarfraFragment();
}

enum PlayerState { stopped, playing, paused }

class _PrarfraFragment extends State<PrarfraFragment>
    with SingleTickerProviderStateMixin {
  bool _isLoading = false;
  WordsImage wordImage;
  WordsAudio wordAudio;
  DictionaryWords wordDictionaryWord;
  DictionaryWords wordDictionaryWordOption_1;
  DictionaryWords wordDictionaryWordOption_2;

  String graphemeTarget;
  String graphemeSource;
  String graphemeSourceOption_1;
  String graphemeSourceOption_2;
  String pathImagen;
  String pathAudio;

  String indice;

  int a, b, c;

  List<String> lista = [];
  List validaButton = [];

  List<String> listaSolucion = [];
  List<int> listaSolucionIndice = [];
  List<String> graphemeS;

  /*AudioPlayer audioPlayer = new AudioPlayer();
  AudioPlayer audioPlugin = new AudioPlayer();
  PlayerState playerState = PlayerState.stopped;*/
  AudioPlayer _audioPlayer;
  PlayerState _playerState = PlayerState.stopped;
  PlayerMode mode;

  Future<String> getJSONData() async {
    wordDictionaryWord = await DataApi()
        .getDictionaryWords(widget.cards[widget.index]['id_key_item']);
    wordDictionaryWordOption_1 = await DataApi()
        .getDictionaryWords(widget.cards[widget.index]['option_dict_word_1']);
    wordDictionaryWordOption_2 = await DataApi()
        .getDictionaryWords(widget.cards[widget.index]['option_dict_word_2']);
    wordAudio =
        await DataApi().getWordAudio(widget.cards[widget.index]['id_key_item']);
    setState(() {
      graphemeTarget = wordDictionaryWord.getGraphemeTarget();
      graphemeSource = wordDictionaryWord.getGraphemeSource();
      if (wordDictionaryWordOption_1 != null)
        graphemeSourceOption_1 = wordDictionaryWordOption_1.getGraphemeSource();
      if (wordDictionaryWordOption_2 != null)
        graphemeSourceOption_2 = wordDictionaryWordOption_2.getGraphemeSource();
      else
        graphemeSourceOption_2 = "";

      graphemeS = graphemeSource.split(' ');

      if (wordImage != null) pathImagen = wordImage.getPath();
      pathAudio = wordAudio.getPath();
      listaAleatoria();
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    print('==============Prarfra=================');
    /*return Padding(
      padding: EdgeInsets.only(top: 0, bottom: 0, left: 0, right: 0),
      child: graphemeSourceOption_1 == null ? _loading() : _prarfra(),
    );*/
    return Padding(
      padding: EdgeInsets.only(top: 0, bottom: 0, left: 0, right: 0),
      child:_prarfra(),
    );
  }

  Widget _prarfra() {
    return Container(
      // height: 200,
      height: MediaQuery.of(context).size.height - 60,
      width: MediaQuery.of(context).size.width,
      child: Card(
          elevation: 3,
          color: Theme.Colors.bodyCard_1,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(8.0),
            ),
          ),
          margin: const EdgeInsets.all(15),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              //mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                _textOption("Arma la frase:"),
                _graphemeTarget(),
                Padding(
                    padding:
                        EdgeInsets.only(top: 30, bottom: 15, left: 0, right: 0),
                    child: Center(
                      child: _solucionFila1(),
                    )),
                Padding(
                    padding:
                        EdgeInsets.only(top: 0, bottom: 15, left: 0, right: 0),
                    child: Center(
                      child: _solucionFila2(),
                    )),
                Padding(
                    padding:
                        EdgeInsets.only(top: 0, bottom: 25, left: 0, right: 0),
                    child: Center(
                      child: _solucionFila3(),
                    )),
                Container(
                  margin:
                      EdgeInsets.only(top: 0, bottom: 0, left: 20, right: 20),
                  decoration: new BoxDecoration(
                    border: Border(
                      top: BorderSide(width: 1.0, color: Colors.green),
                      left: BorderSide(width: 1.0, color: Colors.green),
                      right: BorderSide(width: 1.0, color: Colors.green),
                      bottom: BorderSide(width: 1.0, color: Colors.greenAccent),
                    ),
                  ),
                ),
                Padding(
                    padding:
                        EdgeInsets.only(top: 30, bottom: 15, left: 0, right: 0),
                    child: Center(
                      child: _fila1(),
                    )),
                Padding(
                    padding:
                        EdgeInsets.only(top: 0, bottom: 15, left: 0, right: 0),
                    child: Center(
                      child: _fila2(),
                    )),
                Padding(
                    padding:
                        EdgeInsets.only(top: 0, bottom: 15, left: 0, right: 0),
                    child: Center(
                      child: _fila3(),
                    )),
                Padding(
                    padding:
                        EdgeInsets.only(top: 0, bottom: 25, left: 0, right: 0),
                    child: Center(
                      child: _eventButton(),
                    )),
              ],
            ),
          )),
    );
  }

  Widget _textOption(String texto) {
    return Padding(
      padding: EdgeInsets.only(top: 15, bottom: 8, left: 10, right: 10),
      child: Text(
        texto,
        style: TextStyle(
            fontSize: 25.0,
            color: Theme.Colors.textOption,
            fontWeight: FontWeight.bold),
      ),
    );
  }

  Widget _graphemeTarget() {
    return Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8, left: 10, right: 10),
      child: Text(
        graphemeTarget == null ? '' : graphemeTarget,
        style: TextStyle(
            fontSize: 25.0,
            color: Theme.Colors.textTarget,
            fontWeight: FontWeight.bold),
      ),
    );
  }

  Widget _fila1() {
    return SizedBox(
      height: 30.0,
      child: ListView.builder(
          physics: ClampingScrollPhysics(),
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          //itemCount: 3,
          itemCount: lista.length < 3 ? lista.length : 3,
          itemBuilder: (BuildContext context, int i) {
            return _buttonSelect(lista[i], i);
          }),
    );
  }

  Widget _fila2() {
    return SizedBox(
      height: 30.0,
      child: ListView.builder(
          physics: ClampingScrollPhysics(),
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          // itemCount: lista == null ? 0 : (lista.length)-3,
          itemCount: lista.length < 6 ? (lista.length) - 3 : 3,
          itemBuilder: (BuildContext context, int i) {
            return _buttonSelect(lista[i + 3], i + 3);
          }),
    );
  }

  Widget _fila3() {
    return SizedBox(
      height: 30.0,
      child: ListView.builder(
          physics: ClampingScrollPhysics(),
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemCount: (lista.length) - 6,
          // itemCount: 2,
          itemBuilder: (BuildContext context, int i) {
            return _buttonSelect(lista[i + 6], i + 6);
          }),
    );
  }

  Widget _solucionFila1() {
    return SizedBox(
      height: 30.0,
      child: ListView.builder(
          physics: ClampingScrollPhysics(),
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          //itemCount: 3,
          itemCount: listaSolucion.length < 3 ? listaSolucion.length : 3,
          itemBuilder: (BuildContext context, int i) {
            return _buttonSelectSolucion(listaSolucion[i], i);
          }),
    );
  }

  Widget _solucionFila2() {
    return SizedBox(
      height: 30.0,
      child: ListView.builder(
          physics: ClampingScrollPhysics(),
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          //itemCount: 3,
          itemCount: listaSolucion.length < 6 ? (listaSolucion.length) - 3 : 3,
          itemBuilder: (BuildContext context, int i) {
            return _buttonSelectSolucion(listaSolucion[i + 3], i + 3);
          }),
    );
  }

  Widget _solucionFila3() {
    return SizedBox(
      height: 30.0,
      child: ListView.builder(
          physics: ClampingScrollPhysics(),
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          //itemCount: 3,
          itemCount: (listaSolucion.length) - 6,
          itemBuilder: (BuildContext context, int i) {
            return _buttonSelectSolucion(listaSolucion[i + 6], i + 6);
          }),
    );
  }

  Widget _buttonSelect(String text, int indice) {
    return Padding(
        padding: const EdgeInsets.only(top: 0, bottom: 0, left: 3, right: 3),
        child: SizedBox(
          //height: 10.0,
          child: FlatButton(
            child: Text(
              text,
              textDirection: TextDirection.ltr,
              style: TextStyle(
                  color: Colors.white,
                  fontFamily: "Poppins-Bold",
                  fontSize: 15,
                  letterSpacing: 0),
            ),
            color: Theme.Colors.buttonIniciar,
            disabledColor: Theme.Colors.buttonIniciando,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(5.0)),
            // onPressed: _isLoading ? null : _login,
            onPressed: validaButton[indice]
                ? null
                : () async {
                    setState(() {
                      validaButton[indice] = true;
                      listaSolucion.add(text);
                      listaSolucionIndice.add(indice);
                    });
                    bool sw = true;
                    for (int i = 0; i < listaSolucion.length; i++)
                      if (graphemeS[i] != listaSolucion[i]) sw = false;
                    if (sw && listaSolucion.length==graphemeS.length) {
                      await _audioPlayer.play(pathAudio, isLocal: false);
                      setState(() => _playerState = PlayerState.playing);
                      _showMsg('Tu respuesta es correcta', '');
                      const twentyMillis = const Duration(seconds:3);
                      new Timer(twentyMillis, () =>
                      Navigator.pop(context));
                      new Timer(twentyMillis, () =>
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => 
                      CollectionCards(cards:widget.cards,index:(widget.index)+1))));
                    }
                  },
          ),
        ));
  }

  Widget _buttonSelectSolucion(String text, int indice) {
    return Padding(
        padding: const EdgeInsets.only(top: 0, bottom: 0, left: 3, right: 3),
        child: SizedBox(
          //height: 20.0,
          child: FlatButton(
            child: Text(
              text,
              textDirection: TextDirection.ltr,
              style: TextStyle(
                  color: Colors.white,
                  fontFamily: "Poppins-Bold",
                  fontSize: 15,
                  letterSpacing: 0),
            ),
            color: Theme.Colors.buttonIniciar,
            disabledColor: Theme.Colors.buttonIniciando,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(5.0)),
            // onPressed: _isLoading ? null : _login,
            onPressed: () async {
              setState(() {
                validaButton[listaSolucionIndice[indice]] = false;
                listaSolucion.removeAt(indice);
                listaSolucionIndice.removeAt(indice);
              });
              bool sw = true;
              for (int i = 0; i < listaSolucion.length; i++)
                if (graphemeS[i] != listaSolucion[i]) 
                  sw = false;
              if (sw && listaSolucion.length==graphemeS.length) {
                await _audioPlayer.play(pathAudio, isLocal: false);
                setState(() => _playerState = PlayerState.playing); 
                _showMsg('Tu respuesta es correcta', '');
                const twentyMillis = const Duration(seconds:3);
                new Timer(twentyMillis, () =>
                Navigator.pop(context));
                new Timer(twentyMillis, () =>
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => 
                CollectionCards(cards:widget.cards,index:(widget.index)+1))));
              }
              print(listaSolucion);
            },
          ),
        ));
  }

  void _login() async {
    setState(() {
      _isLoading = true;
    });

    /* setState(() {
       _isLoading = false;
    });*/
  }

  _showMsg(msg, msg2) {
    //
    final snackBar = SnackBar(
        //duration: const Duration(milliseconds: 100),

        backgroundColor: msg2 == ''
            ? Theme.Colors.showMsgCorrecto
            : Theme.Colors.showMsgInCorrecto,
        content: Container(
            height: 150.0,
            width: MediaQuery.of(context).size.width + 20,
            child: Column(
              children: <Widget>[
                msg2 == ''
                    ? Image(
                        image: AssetImage('assets/icons/bien.png'),
                        width: 50,
                        height: 50,
                        fit: BoxFit.cover)
                    : Image(
                        image: AssetImage('assets/icons/mal.png'),
                        fit: BoxFit.cover),
                Text(
                  msg,
                  style: TextStyle(
                      fontSize: 22.0,
                      color: Colors.white,
                      letterSpacing: 2.0,
                      fontWeight: FontWeight.bold),
                ),
                msg2 == ''
                    ? Text(msg2, style: TextStyle(fontSize: 0))
                    : Text(
                        msg2,
                        style: TextStyle(
                            fontSize: 24.0,
                            color: Colors.white,
                            letterSpacing: 1.0,
                            fontWeight: FontWeight.bold),
                      ),
                //_eventButtonContinuar(),
              ],
            )));
    Scaffold.of(context).showSnackBar(snackBar);
  }

  listaAleatoria() {
    // List aux = ['HOLA','GGGGG','W','3'];
    List aux = [''] +
        graphemeSource.split(" ") +
        graphemeSourceOption_1.split(" ") +
        graphemeSourceOption_2.split(" ");
    String cad = '';
    for (var i = aux.length; i >= 1; i--) {
      var rng = new Random();
      int ran = rng.nextInt(i);
      cad = aux[ran] + '&' + cad;
      aux.remove(aux[ran]);
    }
    lista = cad.split('&');
    for (var i = 0; i < lista.length; i++) {
      lista.remove('');
    }
    for (var i = 0; i < lista.length; i++) {
      validaButton.add(false);
    }
    print(lista);
    print(validaButton);
  }

  Widget _eventButton() {
    return OutlineButton(
        borderSide: BorderSide(
          color: Theme.Colors.buttonNext, //Color of the border
          style: BorderStyle.solid, //Style of the border
          width: 3, //width of the border
        ),
        child: Padding(
          padding: EdgeInsets.only(top: 8, bottom: 8, left: 10, right: 10),
          child: Text('COMPROBAR',
              //_isLoading? 'INICIANDO...' : '¡COMENZAR!',
              style: TextStyle(
                  color: Colors.black,
                  fontFamily: "Poppins-Bold",
                  fontSize: 15,
                  letterSpacing: 1.0)),
        ),
        color: Theme.Colors.buttonComezar,
        // disabledColor: Theme.Colors.buttonComezar,
        shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(20.0)),
        onPressed: () async {
          print('==========push PRARFRA==========');
            await _audioPlayer.play('https://back.tecnicos.lenguas.org.bo/raw/wrong.mp3', isLocal: false);
            setState(() => _playerState = PlayerState.playing);
            _showMsg('La respuesta correcta es :', graphemeSource);
      
            const twentyMillis = const Duration(seconds:3);
            new Timer(twentyMillis, () =>
            Navigator.pop(context));
            new Timer(twentyMillis, () =>
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => 
            CollectionCards(cards:widget.cards,index:(widget.index)+1))));
        });
  }

  Widget _loading() {
    return Padding(
      padding: EdgeInsets.only(top: 200, bottom: 0, left: 0, right: 0),
      child: new CircularProgressIndicator(),
    );
  }
  void _initAudioPlayer() {
    _audioPlayer = AudioPlayer(mode: mode);
  }
  @override
  void initState() {
    super.initState();
    // Call the getJSONData() method when the app initializes
    this.getJSONData();
    _initAudioPlayer();
  }
}
