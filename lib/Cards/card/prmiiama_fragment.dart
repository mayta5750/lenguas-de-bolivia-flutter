import 'package:flutter/material.dart';
import 'package:tutorial_project/api/data.dart';
import 'package:cached_network_image/cached_network_image.dart';

import 'dart:async';
import 'dart:math';
import 'package:tutorial_project/Theme.dart' as Theme;
import 'package:tutorial_project/Models/WordsImage.dart';
import 'package:tutorial_project/Models/WordsAudio.dart';
import 'package:tutorial_project/Models/DictionaryWords.dart';
import 'package:flutter_svg/flutter_svg.dart';
//import 'package:audioplayer/audioplayer.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:tutorial_project/Cards/card/card.dart';

class PrmiiamaFragment extends StatefulWidget {
  PrmiiamaFragment({Key key, this.cards, this.index}) : super(key: key);
  var cards;
  int index;
  @override
  _PrmiiamaFragment createState() => _PrmiiamaFragment();
}

enum PlayerState { stopped, playing, paused }

class _PrmiiamaFragment extends State<PrmiiamaFragment>
    with SingleTickerProviderStateMixin {
  WordsImage wordImage;
  WordsImage wordImageWordOption_1;
  WordsImage wordImageWordOption_2;

  WordsAudio wordAudio;
  WordsAudio wordAudioWordOption_1;
  WordsAudio wordAudioWordOption_2;

  DictionaryWords wordDictionaryWord;

  String graphemeTarget;
  String graphemeSource;

  String pathAudio;
  String pathImagen;

  String pathImagenOption_1;
  String pathImagenOption_2;

  String pathAudioOption_1;
  String pathAudioOption_2;

  String indice;

  int a, b, c;

  List list;

  /*AudioPlayer audioPlayer = new AudioPlayer();
  AudioPlayer audioPlugin = new AudioPlayer();
  PlayerState playerState = PlayerState.stopped;*/
  AudioPlayer _audioPlayer;
  PlayerState _playerState = PlayerState.stopped;
  PlayerMode mode;

  Future<String> getJSONData() async {
    print(widget.cards);
    wordDictionaryWord = await DataApi().getDictionaryWords(widget.cards[widget.index]['id_key_item']);
    wordAudio = await DataApi().getWordAudio(widget.cards[widget.index]['id_key_item']);
    /*wordAudioWordOption_1 = await DataApi().getWordAudio(widget.cards[widget.index]['option_dict_word_1']);
    wordAudioWordOption_2 = await DataApi().getWordAudio(widget.cards[widget.index]['option_dict_word_2']);*/
      wordImage = await DataApi().getWordImage(widget.cards[widget.index]['id_key_item']);
      wordImageWordOption_1 = await DataApi().getWordImage(widget.cards[widget.index]['option_dict_word_1']);
      wordImageWordOption_2 = await DataApi().getWordImage(widget.cards[widget.index]['option_dict_word_2']);


    setState(() {
      graphemeTarget = wordDictionaryWord.getGraphemeTarget();
      graphemeSource = wordDictionaryWord.getGraphemeSource();
      if(wordImage!=null)
        pathImagen = wordImage.getPath();
      if(wordImageWordOption_1!=null)
        pathImagenOption_1 = wordImageWordOption_1.getPath();
      if(wordImageWordOption_2!=null)
        pathImagenOption_2 = wordImageWordOption_2.getPath();
      pathAudio = wordAudio.getPath();
      indice = aleatorio();
      a = int.parse(indice.substring(0, 1));
      b = int.parse(indice.substring(1, 2));
      c = int.parse(indice.substring(2, 3));
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    print('==============PRMIIAMA=================');
    return Padding(
      padding: EdgeInsets.only(top: 0, bottom: 0, left: 0, right: 0),
      child: wordImageWordOption_1 == null
          ? _loading()
          : _prmiiama(),
    );
    /*return Padding(
      padding: EdgeInsets.only(top: 0, bottom: 0, left: 0, right: 0),
      child:  _prmiiama(),
    );*/
  }

  Widget _prmiiama() {
    return Container(
      // height: 200,
      height: MediaQuery.of(context).size.height - 60,
      width: MediaQuery.of(context).size.width,
      child: Card(
        elevation: 3,
        color: Theme.Colors.bodyCard_1,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(8.0),
          ),
        ),
        margin: const EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 30, bottom: 10, left: 0, right: 0),
               child:wordDictionaryWord==null?_textTarget(''):_textTarget(graphemeTarget),
              ),
              Padding(
                padding: EdgeInsets.only(top: 0, bottom: 35, left: 0, right: 0),
                child: _trioImagen(),
              ),
            ],
          ),
        )

        
      ),
    );
  }

  Widget _textTarget(String texto) {
    return  Text(
        texto,
        textAlign: TextAlign.center,
        style: TextStyle(
            fontSize: 24.0,
            color: Theme.Colors.textTarget,
            fontWeight: FontWeight.bold),
      
    );
  }

  Widget _trioImagen() {
    List aux = [
      wordImage == null ? '' : pathImagen,
      wordImageWordOption_1 == null ? '' : pathImagenOption_1,
      wordImageWordOption_2 == null ? '' : pathImagenOption_2,
    ];
    return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        //mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Column(
            children: <Widget>[_imagenJpg(aux[a]), _button(aux[a],a)],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Column(children: <Widget>[_imagenJpg(aux[b]), _button(aux[b],b)]),
              Column(children: <Widget>[_imagenJpg(aux[c]), _button(aux[c],c)]),
            ],
          )
        ]
      );
  }

  Widget _audio(String path) {
    return Container(
        margin: const EdgeInsets.all(15.0),
        decoration: BoxDecoration(
          border: Border.all(
            color: Colors.green,
          ),
          borderRadius: BorderRadius.all(Radius.circular(9.0)),
        ),
        width: 130,
        height: 130,
        child: Center(
            child: Align(
          alignment: Alignment.center,
          child: FloatingActionButton(
            onPressed: () async {
            await _audioPlayer.play(path, isLocal: false);
            setState(() => _playerState = PlayerState.playing);
            },
            tooltip: 'Play',
            child: const Icon(Icons.play_arrow),
          ),
        )
      )
    );
  }

  Widget _imagenJpg(String path) {
    if(path.isEmpty){
      return Container(
          margin: const EdgeInsets.all(15.0),
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.green,
            ),
            borderRadius: BorderRadius.all(Radius.circular(9.0)),
          ),
          width: 130,
          height: 130,
          child: Center(
              child: Align(
            alignment: Alignment.center,
            child: Text(graphemeSource)
          )
        )
      );
    }else{
      if(path.substring(path.length-3)=='svg'){
        return Container(
          margin: const EdgeInsets.all(15.0),
          child:new ClipRRect(
            borderRadius: BorderRadius.circular(10.0),
              child:SvgPicture.network(
              path,
              semanticsLabel: 'Feed button',
              height: 130.0,
              width: 130.00,
              fit: BoxFit.contain,
            )
          )
        );
      } else {
        return Container(
          margin: const EdgeInsets.all(15.0),
          child: new  ClipRRect(
            borderRadius: BorderRadius.circular(10.0),
            child:CachedNetworkImage(      
              imageUrl: path,
              height: 130.0,
              width: 130.00,
            ),
          )
        );
      }            
    }                          
  }
  Widget _button(String text, int indice) {
    return SizedBox(
      child: new RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(10.0),
          // side: BorderSide(color: Colors.red)
        ),
        child: Text('SELECCIONAR',
            style: TextStyle(
                color: Colors.white,
                fontFamily: "Poppins-Bold",
                fontSize: 15,
                letterSpacing: 1.0)),
        elevation: 5.0,
        color: Theme.Colors.collectionButtons,
        onPressed: () async {
          print(text);
          print(pathImagen);
          if (text == pathImagen || indice==0) {
            await _audioPlayer.play(pathAudio, isLocal: false);
            setState(() => _playerState = PlayerState.playing);
            _showMsg('Tu respuesta es correcta', '');
          } else {
            await _audioPlayer.play('https://back.tecnicos.lenguas.org.bo/raw/wrong.mp3', isLocal: false);
            setState(() => _playerState = PlayerState.playing);
            _showMsg('Tu respuesta es incorrecta', ' ');
          }
          const twentyMillis = const Duration(seconds: 2);
          new Timer(twentyMillis, () => Navigator.pop(context));
          new Timer(
              twentyMillis,
              () => Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => CollectionCards(
                      cards: widget.cards, index: (widget.index) + 1))));
        },
      ),
    );
  }

  _showMsg(msg, msg2) {
    //
    final snackBar = SnackBar(
        duration: const Duration(milliseconds: 1000),
        backgroundColor: msg2 == ''
            ? Theme.Colors.showMsgCorrecto
            : Theme.Colors.showMsgInCorrecto,
        content: Container(
            height: 150.0,
            width: MediaQuery.of(context).size.width + 20,
            child: Column(
              children: <Widget>[
                msg2 == ''
                    ? Image(
                        image: AssetImage('assets/icons/bien.png'),
                        width: 50,
                        height: 50,
                        fit: BoxFit.cover)
                    : Image(
                        image: AssetImage('assets/icons/mal.png'),
                        fit: BoxFit.cover),
                Text(
                  msg,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 22.0,
                      color: Colors.white,
                      letterSpacing: 2.0,
                      fontWeight: FontWeight.bold),
                ),
                msg2 == ''
                    ? Text(msg2, style: TextStyle(fontSize: 0))
                    : Text(
                        msg2,
                        style: TextStyle(
                            fontSize: 24.0,
                            color: Colors.white,
                            letterSpacing: 1.0,
                            fontWeight: FontWeight.bold),
                      ),
                //_eventButtonContinuar(),
              ],
            )));
    Scaffold.of(context).showSnackBar(snackBar);
  }
  aleatorio() {
    var rng = new Random();
    int ran = rng.nextInt(5);
    switch (ran) {
      case 0:
        return '012';
      case 1:
        return '021';
      case 2:
        return '102';
      case 3:
        return '120';
      case 4:
        return '201';
      case 5:
        return '210';
    }
  }
  Widget _loading(){
    return  Padding(
      padding: EdgeInsets.only(top: 200, bottom: 0, left: 0, right: 0),
      child: new CircularProgressIndicator(),
    );
  }
  void _initAudioPlayer() {
    _audioPlayer = AudioPlayer(mode: mode);
  }
  @override
  void initState() {
    super.initState();
    // Call the getJSONData() method when the app initializes
    this.getJSONData();
    _initAudioPlayer();
  }
}
