import 'package:flutter/material.dart';
import 'package:tutorial_project/api/data.dart';
import 'package:cached_network_image/cached_network_image.dart';

import 'dart:async';
import 'dart:math';
import 'package:tutorial_project/Theme.dart' as Theme;
import 'package:tutorial_project/Models/WordsImage.dart';
import 'package:tutorial_project/Models/WordsAudio.dart';
import 'package:tutorial_project/Models/DictionaryWords.dart';
import 'package:flutter_svg/flutter_svg.dart';
//import 'package:audioplayer/audioplayer.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:tutorial_project/Cards/card/card.dart';

class PrseiamaFragment extends StatefulWidget {
  PrseiamaFragment({Key key, this.cards, this.index}) : super(key: key);
  var cards;
  int index;
  @override
  _PrseiamaFragment createState() => _PrseiamaFragment();
}

enum PlayerState { stopped, playing, paused }

class _PrseiamaFragment extends State<PrseiamaFragment>
    with SingleTickerProviderStateMixin {
  WordsImage wordImage;
  WordsAudio wordAudio;
  DictionaryWords wordDictionaryWord;
  DictionaryWords wordDictionaryWordOption_1;
  DictionaryWords wordDictionaryWordOption_2;
  DictionaryWords wordDictionaryWordOption_3;


  String graphemeTarget;
  String graphemeSource;
  String graphemeSourceOption_1;
  String graphemeSourceOption_2;
  String graphemeSourceOption_3;

  String pathImagen;
  String pathAudio;

  String indice;

  int a, b, c;

  List <String> lista=[];

  /*AudioPlayer audioPlayer = new AudioPlayer();
  AudioPlayer audioPlugin = new AudioPlayer();
  PlayerState playerState = PlayerState.stopped;*/
  AudioPlayer _audioPlayer;
  PlayerState _playerState = PlayerState.stopped;
  PlayerMode mode;

  Future<String> getJSONData() async {
    // print(widget.cards);
    wordDictionaryWord = await DataApi().getDictionaryWords(widget.cards[widget.index]['id_key_item']);
    wordDictionaryWordOption_1 = await DataApi().getDictionaryWords(widget.cards[widget.index]['option_dict_word_1']);
    wordDictionaryWordOption_2 = await DataApi().getDictionaryWords(widget.cards[widget.index]['option_dict_word_2']);
    wordDictionaryWordOption_3 = await DataApi().getDictionaryWords(widget.cards[widget.index]['option_dict_word_3']);
    wordImage = await DataApi().getWordImage(widget.cards[widget.index]['id_key_item']);
    wordAudio = await DataApi().getWordAudio(widget.cards[widget.index]['id_key_item']);
    setState(() {
      graphemeTarget = wordDictionaryWord.getGraphemeTarget();
      graphemeSource = wordDictionaryWord.getGraphemeSource();
      graphemeSourceOption_1 = wordDictionaryWordOption_1==null? '' : wordDictionaryWordOption_1.getGraphemeSource();
      graphemeSourceOption_2 = wordDictionaryWordOption_2==null? '' : wordDictionaryWordOption_2.getGraphemeSource();
      graphemeSourceOption_3 = wordDictionaryWordOption_3==null? '' : wordDictionaryWordOption_3.getGraphemeSource();
      if(wordImage!=null)
        pathImagen = wordImage.getPath();
      pathAudio = wordAudio.getPath();
      listaAleatoria();
     // lista = [graphemeSource, graphemeSourceOption_1, graphemeSourceOption_2,graphemeSourceOption_3];
      print(lista);
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    print('==============Prseiama=================');
    //return _prseiama();
    /*return Padding(
      padding: EdgeInsets.only(top: 0, bottom: 0, left: 0, right: 0),
      child: graphemeSourceOption_1 == null
          ? _loading()
          : _prseiama(),
    );*/
    return Padding(
      padding: EdgeInsets.only(top: 0, bottom: 0, left: 0, right: 0),
      child: _prseiama(),
    );
  }

  Widget _prseiama() {
    return Container(
      height: MediaQuery.of(context).size.height - 60,
      width: MediaQuery.of(context).size.width,
      child: Card(
        elevation: 3,
        color: Theme.Colors.bodyCard_1,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(8.0),
          ),
        ),
        margin: const EdgeInsets.all(20),
        child: SingleChildScrollView(
          child:Column(
            children: <Widget>[
             _textOption("Escoge el significado de:"),
             wordImage==null?_textOption(""):_graphemeTarget(),
             _imagenJpg(),
            
             _collectionButtons(),
            
          ],
          ),
        ),
      ),
    );
  }

  Widget _textOption(String texto) {
    return Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8, left: 10, right: 10),
      child: Text(
        texto,
        style: TextStyle(
            fontSize: 22.0,
            color: Theme.Colors.textOption,
            fontWeight: FontWeight.bold),
      ),
    );
  }

  Widget _graphemeTarget() {
    return Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8, left: 10, right: 10),
      child: Text(
        graphemeTarget == null ? '' : graphemeTarget,
        textAlign: TextAlign.center,
        style: TextStyle(
            
            fontSize: 25.0,
            color: Theme.Colors.textTarget,
            fontWeight: FontWeight.bold),
      ),
    );
  }
  Widget _graphemeSource() {
    return Padding(
        padding: EdgeInsets.only(top: 8, bottom: 8, left: 10, right: 10),
        child: Text(
          graphemeSource == null ? '' : graphemeSource,
          style: TextStyle(
              fontSize: 28.0,
              color: Theme.Colors.textSource,
              fontWeight: FontWeight.bold),
        )
    );
  }
    Widget _imagenJpg() {
    print(pathImagen);
    if(wordImage==null){
    return Container(
        margin: const EdgeInsets.all(5.0),
        decoration: BoxDecoration(
          border: Border.all(
            color: Colors.green,
          ),
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
        ),
        width: 200,
        height: 200,
        child: Center(
            child: Align(
          alignment: Alignment.center,
          child: _graphemeTarget()
        )
      )
    );
    }else{
      if(pathImagen.substring(pathImagen.length-3)=='svg'){
      return Container(
        margin: const EdgeInsets.all(5.0),
        child:new ClipRRect(
        borderRadius: BorderRadius.circular(10.0),
        child:new SvgPicture.network(
        pathImagen,
        semanticsLabel: 'Feed button',
        height: 200.0,
        width: 200.00,
        fit: BoxFit.contain,
      )
      )
      );
      } else {
        return Container(
        margin: const EdgeInsets.all(5.0),
        child:new ClipRRect(
        borderRadius: BorderRadius.circular(10.0),
        child:new CachedNetworkImage(                
                    imageUrl: pathImagen,
                    height: 200.0,
                    width: 200.00,
            )
          )
        );
      }            
    }           
  }

  Widget _collectionButtons() {
    return SizedBox(
     // height: 30.0,
      child: ListView.builder(
          physics: ClampingScrollPhysics(),
          shrinkWrap: true,
          itemCount: (lista.length),
          itemBuilder: (BuildContext context, int i) {
            return _buttonPrseiama(lista[i]);
          }
      ),
    );
  }

  Widget _buttonPrseiama(String text) {
    return Center(
        child: SizedBox(
      width: MediaQuery.of(context).size.width - 80,
      child: new RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(10.0),
          // side: BorderSide(color: Colors.red)
        ),
        child: Text(text,
            style: TextStyle(
                color: Colors.white,
                fontFamily: "Poppins-Bold",
                fontSize: 17,
                letterSpacing: 1.0)),
        elevation: 5.0,
        color: Theme.Colors.collectionButtons,
        onPressed: () async {
          if (text == graphemeSource) {
            _showMsg('Tu respuesta es correcta', '');
            await _audioPlayer.play(pathAudio, isLocal: false);
            setState(() => _playerState = PlayerState.playing);
          } else {
            await _audioPlayer.play('https://back.tecnicos.lenguas.org.bo/raw/wrong.mp3', isLocal: false);
            setState(() => _playerState = PlayerState.playing);            
            _showMsg('La respuesta correcta es :', graphemeSource);
          }
          const twentyMillis = const Duration(seconds: 2);
          new Timer(twentyMillis, () => Navigator.pop(context));
          new Timer(
              twentyMillis,
              () => Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => CollectionCards(
                      cards: widget.cards, index: (widget.index) + 1))));
        },
      ),
    ));
  }

  _showMsg(msg, msg2) {
    //
    final snackBar = SnackBar(
        duration: const Duration(milliseconds: 1000),
        backgroundColor: msg2 == ''
            ? Theme.Colors.showMsgCorrecto
            : Theme.Colors.showMsgInCorrecto,
        content: Container(
            height: 150.0,
            width: MediaQuery.of(context).size.width + 20,
            child: Column(
              children: <Widget>[
                msg2 == ''
                    ? Image(
                        image: AssetImage('assets/icons/bien.png'),
                        width: 50,
                        height: 50,
                        fit: BoxFit.cover)
                    : Image(
                        image: AssetImage('assets/icons/mal.png'),
                        fit: BoxFit.cover),
                Text(
                  msg,
                  style: TextStyle(
                      fontSize: 22.0,
                      color: Colors.white,
                      letterSpacing: 2.0,
                      fontWeight: FontWeight.bold),
                ),
                msg2 == ''
                    ? Text(msg2, style: TextStyle(fontSize: 0))
                    : Text(
                        msg2,
                        style: TextStyle(
                            fontSize: 24.0,
                            color: Colors.white,
                            letterSpacing: 1.0,
                            fontWeight: FontWeight.bold),
                      ),
                //_eventButtonContinuar(),
              ],
            )));
    Scaffold.of(context).showSnackBar(snackBar);
  }

  /*aleatorio() {
    var rng = new Random();
    int ran = rng.nextInt(5);
    switch (ran) {
      case 0:
        return '012';
      case 1:
        return '021';
      case 2:
        return '102';
      case 3:
        return '120';
      case 4:
        return '201';
      case 5:
        return '210';
    }
  }*/
  Widget _loading(){
    return  Padding(
      padding: EdgeInsets.only(top: 200, bottom: 0, left: 0, right: 0),
      /*child:Image(
          image: AssetImage('assets/ops/loading.png'),
          width: 200,
          height: 200,
          fit: BoxFit.cover
      ),*/
      child: new CircularProgressIndicator(),
    );
  }
  listaAleatoria() {
    List aux = [graphemeSource, graphemeSourceOption_1, graphemeSourceOption_2,graphemeSourceOption_3];
   
    String cad = '';
    for (var i = aux.length; i >= 1; i--) {
      var rng = new Random();
      int ran = rng.nextInt(i);
      cad = aux[ran] + '&' + cad;
      aux.remove(aux[ran]);
    }
    lista = cad.split('&');
    for (var i = 0; i < lista.length; i++) {
      lista.remove('');
    }
    print(lista);
  }
  void _initAudioPlayer() {
    _audioPlayer = AudioPlayer(mode: mode);
  }
  @override
  void initState() {
    super.initState();
    // Call the getJSONData() method when the app initializes
    this.getJSONData();
    _initAudioPlayer();
  }
}
