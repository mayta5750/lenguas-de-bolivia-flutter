import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:tutorial_project/Theme.dart' as Theme;
import 'package:audioplayer/audioplayer.dart';
import 'dart:convert';

class SearchList extends StatelessWidget {
  SearchList({Key key, this.idioma, this.id_version}) : super(key: key);
  final String idioma;
  final String id_version;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(idioma),
        backgroundColor: Theme.Colors.barBackground,
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            //color: Colors.black,
            onPressed: () => Navigator.pop(context)),
      ),
      body: MySearchListPage(id_version: id_version, idioma: idioma),
    );
  }
}

class MySearchListPage extends StatefulWidget {
  MySearchListPage({Key key, this.idioma, this.id_version}) : super(key: key);
  final String idioma;
  final String id_version;
  @override
  _MySearchListPageState createState() => _MySearchListPageState();
}

enum PlayerState { stopped, playing, paused }

class _MySearchListPageState extends State<MySearchListPage> {
  List data;
  AudioPlayer audioPlayer = new AudioPlayer();
  AudioPlayer audioPlugin = new AudioPlayer();
  PlayerState playerState = PlayerState.stopped;
  TextEditingController editingController = TextEditingController();
  String search;
  // Function to get the JSON data
  Future<String> getJSONData() async {
    var response = await http.get(
        Uri.encodeFull("https://back.tecnicos.lenguas.org.bo/api/v1/client/search/Guarani/es_/duele"),
        headers: {"Accept": "application/json"}
    );
    setState(() {
      // Get the JSON data
      data = json.decode(response.body);
    });
    return "Successfull";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.Colors.bodyBackground,
      body: Container(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: TextField(
                onChanged: (value) {
                  search = value;
                },
                controller: editingController,
                decoration: InputDecoration(
                    labelText: "Busqueda",
                    hintText: "Castellano",
                    // prefixIcon: Icon(Icons.search),
                    suffix: IconButton(
                        icon: Icon(Icons.search),
                        onPressed: () async {
                          var response = await http.get(
                              Uri.encodeFull(
                                  "https://back.tecnicos.lenguas.org.bo/api/v1/client/search/"+widget.idioma+"/es_/"+search),
                              headers: {"Accept": "application/json"});
                          setState(() {
                            data = json.decode(response.body);
                          });
                        }),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(25.0)))),
              ),
            ),
            Text(data==null?'Cargando...':(data.length==0?'No se encontraron resultados.':'Resultados en ' + widget.idioma),
                style: TextStyle(
                  //color: Colors.white,
                  fontWeight: FontWeight.normal,
                  fontFamily: "Poppins-Bold",
                  fontSize: 16,
                  letterSpacing: 0.0,
                  //wordSpacing: 20.0,
                )),
            Expanded(
              child: _buildListView(),
            ),
          ],
        ),
      ),
    );
    // _buildListView(),
  }

  Widget _buildListView() {
    return ListView.builder(
        padding: const EdgeInsets.all(16.0),
        itemCount: data == null ? 0 : data.length,
        itemBuilder: (context, index) {
          return _cardRow(data[index]);
          // return _buildRow(data[index]);
        });
  }

  Widget _cardRow(dynamic item) => Container(
        padding: const EdgeInsets.all(5.0),
        decoration: BoxDecoration(color: Colors.white54),
        margin: const EdgeInsets.all(0),
        child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            color: Theme.Colors.searchCard,
            child: Row(
              children: <Widget>[
                Expanded(
                  child: GestureDetector(child: _buildRow(item)),
                ),
                Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10.0),
                    ),
                  ),
                  color: Theme.Colors.searchIcon,
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: FloatingActionButton(
                      onPressed: () async {
                        await audioPlayer
                            .play(item == null ? '' : "http://web.oei.bo/dclo_tecnicos/audios/"+item['file_path']);
                        setState(() {
                          playerState = PlayerState.playing;
                        });
                      },
                      tooltip: 'Play',
                      child: const Icon(Icons.play_arrow),
                    ),
                  ),
                ),
              ],
            )),
      );
  Widget _buildRow(dynamic item) {
    return Column(
      children: <Widget>[
    ListTile(
        title: Text("Castellano :",
            style: TextStyle(
              //color: Colors.white,
              fontWeight: FontWeight.bold,
              fontFamily: "Poppins-Bold",
              fontSize: 16,
              letterSpacing: 0.0,
              //wordSpacing: 20.0,
            )),
        subtitle: Text(item['grapheme_t'] == null ? '' : item['grapheme_t'],
            style: TextStyle(
              //color: Colors.white,
              fontWeight: FontWeight.normal,
              fontFamily: "Poppins-Bold",
              fontSize: 16,
              letterSpacing: 0.0,
            )
          ),
    ),
      ListTile(
        title: Text(widget.idioma+" :",
            style: TextStyle(
              //color: Colors.white,
              fontWeight: FontWeight.bold,
              fontFamily: "Poppins-Bold",
              fontSize: 16,
              letterSpacing: 0.0,
              //wordSpacing: 20.0,
            )),
        subtitle: Text(item['grapheme_source'] == null ? '' : item['grapheme_source'],
            style: TextStyle(
              //color: Colors.white,
              fontWeight: FontWeight.normal,
              fontFamily: "Poppins-Bold",
              fontSize: 16,
              letterSpacing: 0.0,
            )
          ),
    )
    ],);

  }

  _onSearch() async {
    var response = await http.get(
        Uri.encodeFull(
            "https://back.tecnicos.lenguas.org.bo/api/v1/client/search/Guarani/es_/duele"),
        headers: {"Accept": "application/json"});
    setState(() {
      // Get the JSON data
      data = json.decode(response.body);
    });
    return "Successfull";
  }

  @override
  void initState() {
    super.initState();
    // Call the getJSONData() method when the app initializes
    this.getJSONData();
  }
}
